<?php

namespace Redenge\Coupon\FrontModule\Entity;

use DateTime;


/**
 * Description of Coupon
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Coupon extends Entity
{

	/**
	 * ID položky kupónu
	 * @var type 
	 */
	private $itemId;

	/**
	 * Maximální počet použití na konkrétním kódu kupónu
	 * @var int
	 */
	private $itemMaxUse;

	/**
	 * Počet použití konkrétního kódu kupónu
	 * @var type 
	 */
	private $itemUsed;

	/**
	 * @var string
	 */
	private $itemCode;

	/**
	 * @var int
	 */
	private $multishopId;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var int
	 */
	private $type;

	/**
	 * @var int
	 */
	private $validtype;

	/**
	 * @var DateTime
	 */
	private $validFrom;

	/**
	 * @var DateTime
	 */
	private $validTo;

	/**
	 * Maximální počet použití na celé kolekci kódů kupónů
	 * @var int
	 */
	private $numberOfUses;

	/**
	 * Celkem použito na celé kolekci kódu kupónů
	 * @var int
	 */
	private $used;

	/**
	 * @var bool
	 */
	private $active;

	/**
	 * @var float
	 */
	private $priceFrom;

	/**
	 * @var float
	 */
	private $discount;

	/**
	 * @var int
	 */
	private $discountPercent;

	/**
	 * @var int
	 */
	private $discountPercentPricelevel;

	/**
	 * @var int
	 */
	private $discountPercentPromotion;

	/**
	 * @var int
	 */
	private $discountPercentMax;

	/**
	 * @var bool
	 */
	private $isDateValid;

	/**
	 * @var float
	 */
	private $totalDiscountApplied;

	/**
	 * @var int
	 */
	private $validationResult;


	public function getItemId()
	{
		return $this->itemId;
	}


	public function getItemCode()
	{
		return $this->itemCode;
	}


	public function getMultishopId()
	{
		return $this->multishopId;
	}


	public function getCode()
	{
		return $this->code;
	}


	public function getName()
	{
		return $this->name;
	}


	public function getType()
	{
		return $this->type;
	}


	public function getValidtype()
	{
		return $this->validtype;
	}


	public function getValidFrom()
	{
		return $this->validFrom;
	}


	public function getValidTo()
	{
		return $this->validTo;
	}


	public function getNumberOfUses()
	{
		return $this->numberOfUses;
	}


	public function isActive()
	{
		return $this->active;
	}


	public function getPriceFrom()
	{
		return $this->priceFrom;
	}


	public function getDiscount()
	{
		return $this->discount;
	}


	public function getDiscountPercent()
	{
		return $this->discountPercent;
	}


	public function getDiscountPercentPricelevel()
	{
		return $this->discountPercentPricelevel;
	}


	public function getDiscountPercentPromotion()
	{
		return $this->discountPercentPromotion;
	}


	/**
	 * @return int
	 */
	public function getDiscountPercentMax()
	{
		return $this->discountPercentMax;
	}


	public function getUsed()
	{
		return $this->used;
	}


	public function getItemUsed()
	{
		return $this->itemUsed;
	}


	public function getItemMaxUse()
	{
		return $this->itemMaxUse;
	}


	public function getTotalDiscountApplied()
	{
		return $this->totalDiscountApplied;
	}


	public function getValidationResult()
	{
		return $this->validationResult;
	}


	public function isDateValid()
	{
		return $this->isDateValid;
	}


	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
	}


	public function setItemCode($itemCode)
	{
		$this->itemCode = $itemCode;
	}


	public function setMultishopId($multishopId)
	{
		$this->multishopId = $multishopId;
	}


	public function setCode($code)
	{
		$this->code = $code;
	}


	public function setName($name)
	{
		$this->name = $name;
	}


	public function setType($type)
	{
		$this->type = $type;
	}


	public function setValidtype($validtype)
	{
		$this->validtype = $validtype;
	}


	public function setValidFrom($validFrom)
	{
		$this->validFrom = DateTime::createFromFormat('Y-m-d H:i:s', $validFrom);
	}


	public function setValidTo($validTo)
	{
		$this->validTo = DateTime::createFromFormat('Y-m-d H:i:s', $validTo);
	}


	public function setNumberOfUses($numberOfUses)
	{
		$this->numberOfUses = $numberOfUses;
	}


	public function setActive($active)
	{
		$this->active = (bool) $active;
	}


	public function setPriceFrom($priceFrom)
	{
		$this->priceFrom = $priceFrom;
	}


	public function setDiscount($discount)
	{
		$this->discount = $discount;
	}


	public function setDiscountPercent($discountPercent)
	{
		$this->discountPercent = $discountPercent;
	}


	public function setDiscountPercentPricelevel($discountPercentPricelevel)
	{
		$this->discountPercentPricelevel = $discountPercentPricelevel;
	}


	public function setDiscountPercentPromotion($discountPercentPromotion)
	{
		$this->discountPercentPromotion = $discountPercentPromotion;
	}


	public function setDiscountPercentMax($discountPercentMax)
	{
		$this->discountPercentMax = $discountPercentMax;
	}


	public function setUsed($used)
	{
		$this->used = (int) $used;
	}


	public function setItemUsed($itemUsed)
	{
		$this->itemUsed = $itemUsed;
	}


	public function setItemMaxUse($itemMaxUse)
	{
		$this->itemMaxUse = $itemMaxUse;
	}


	public function setIsDateValid($isDateValid)
	{
		$this->isDateValid = (bool) $isDateValid;
	}


	public function setTotalDiscountApplied($discount)
	{
		$this->totalDiscountApplied = $discount;
	}


	public function setValidationResult($result)
	{
		$this->validationResult = $result;
	}

}
