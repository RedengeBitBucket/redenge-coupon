<?php

namespace Redenge\Coupon\FrontModule\Entity;


/**
 * Description of ICouponCart
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponCart
{

	public function getId();

	public function getTotalPrice();

	public function getFormatPrice($price);

	/**
	 * @param array $ids
	 * @return CartProduct[]
	 */
	public function getCouponProducts(array $ids);
}
