<?php

namespace Redenge\Coupon\FrontModule\Entity;

/**
 * Description of CartProduct
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CartProduct extends Entity
{

	/**
	 * @var int
	 */
	private $productId;

	/**
	 * @var int
	 */
	private $quantity;

	/**
	 * Prodejní cena
	 * @var float
	 */
	private $price;

	/**
	 * Doporučená cena
	 * @var float
	 */
	private $priceRetail;

	/**
	 * Je na cenu aplikována promoce?
	 * @var bool
	 */
	private $promotionDiscount;

	/**
	 * Cena po uplatnění kupónu
	 * @var float
	 */
	private $priceCoupon;


	public function isPriceLevelDiscount()
	{
		return $this->priceRetail > 0 && $this->priceRetail > $this->price;
	}


	public function isPromotionDiscount()
	{
		return $this->promotionDiscount;
	}


	public function getProductId()
	{
		return $this->productId;
	}


	public function getQuantity()
	{
		return $this->quantity;
	}


	public function getPrice()
	{
		return $this->price;
	}


	public function getPriceRetail()
	{
		return $this->priceRetail;
	}


	public function getPriceCoupon()
	{
		return $this->priceCoupon;
	}


	public function setProductId($productId)
	{
		$this->productId = $productId;
	}


	public function setQuantity($quantity)
	{
		$this->quantity = (int) $quantity;
	}


	public function setPrice($price)
	{
		$this->price = (float) $price;
	}


	public function setPriceRetail($priceRetail)
	{
		$this->priceRetail = (float) $priceRetail;
	}


	public function setPromotionDiscount($promotionDiscount)
	{
		$this->promotionDiscount = (bool) $promotionDiscount;
	}


	public function setPriceCoupon($priceCoupon)
	{
		$this->priceCoupon = $priceCoupon;
	}


}
