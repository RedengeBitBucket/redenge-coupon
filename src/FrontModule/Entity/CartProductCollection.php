<?php

namespace Redenge\Coupon\FrontModule\Entity;

/**
 * Description of CartProductCollection
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CartProductCollection implements \IteratorAggregate
{

	/**
	 * @var CartProduct[]
	 */
	private $products = [];

	/**
	 * @var int
	 */
	private $index = 1;


	public function addProduct(CartProduct $cartProduct)
	{
		$index = $this->search($cartProduct);
		if ($index === false) {
			$this->products[$this->index] = $cartProduct;
			return $this->index++;
		} else {
			$this->addQuantity($index, $cartProduct->getQuantity());
			return $index;
		}
	}


	public function search(CartProduct $cartProduct)
	{
		foreach ($this->products as $index => $product) {
			if ($product->getProductId() === $cartProduct->getProductId()
				&& $product->getPrice() === $cartProduct->getPrice()
				&& $product->isPromotionDiscount() === $cartProduct->isPromotionDiscount()
			) {
				return $index;
			}
		}

		return false;
	}


	public function addQuantity($index, $quantity)
	{
		if (!isset($this->products[$index])) {
			return;
		}
		$this->products[$index]->setQuantity($this->products[$index]->getQuantity() + $quantity);
	}


	/**
	 * @return CartProduct[]
	 */
	public function getIterator()
	{
		return new \ArrayIterator($this->products);
	}

}
