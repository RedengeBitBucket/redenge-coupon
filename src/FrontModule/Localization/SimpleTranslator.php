<?php

namespace Redenge\Coupon\FrontModule\Localization;

use Nette;

class SimpleTranslator extends Nette\Object implements Nette\Localization\ITranslator
{

	/**
	 * @var array
	 */
	private $dictionary = [
		'redenge_coupon.required' => 'Povinné pole',
		'redenge_coupon.label_coupon' => 'Kupón',
		'redenge_coupon.save' => 'Potvrdit',
		'redenge_coupon.enter_code' => 'Zadejte kód',
		'redenge_coupon.modal_title' => 'Vyhodnocení kupónu',
		'redenge_coupon.add_success' => 'Do košíku byl vložen akční kupón ,,%s"',
		'redenge_coupon.not_exists' => 'Kupón neexistuje',
		'redenge_coupon.error_max_used' => 'Kupón dosáhl maximálního počtu užití',
		'redenge_coupon.error_validity_expired' => 'Platnost kupónu vypršela',
		'redenge_coupon.error_price_from' => 'Kupón lze uplatnit pouze při překročení částky %s za produkty',
		'redenge_coupon.error_discount_is_bigger_than_total_price' => 'Velikost slevy kupónu je větší než cena produktů v košíku',
		'redenge_coupon.error_discount_is_bigger_than_products_price' => 'Velikost slevy kupónu je větší než cena produktů v košíku',
		'redenge_coupon.no_products_for_discount' => 'Nákup neobsahuje žádné produkty, které splňují kritéria pro uplatnění kupónu',
		'redenge_coupon.no_discount' => 'Kupón neobsahuje žádnou slevu na produkty v košíku',
		'redenge_coupon.discount_percent_max_info' => 'Produkt může být slevněn maximálně do %d%% ceny produktu.'
	];


	/**
	 * @param array $dictionary
	 */
	public function __construct($dictionary = NULL)
	{
		if (is_array($dictionary)) {
			$this->dictionary = $dictionary;
		}
	}


	/**
	 * Translates the given string
	 * 
	 * @param  string
	 * @param  int
	 * @return string
	 */
	public function translate($message, $count = NULL)
	{
		return isset($this->dictionary[$message]) ? $this->dictionary[$message] : $message;
	}


	/**
	 * Set translator dictionary
	 * @param array $dictionary
	 */
	public function setDictionary(array $dictionary)
	{
		$this->dictionary = $dictionary;
	}

}
