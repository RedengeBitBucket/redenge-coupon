<?php

namespace Redenge\Coupon\FrontModule;

use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Redenge\Coupon\FrontModule\Entity\Coupon;
use Redenge\Coupon\FrontModule\Entity\ICouponCart;
use Redenge\Coupon\FrontModule\Repository\CouponRepository;
use Redenge\Coupon\FrontModule\CouponValidator;
use Redenge\Coupon\FrontModule\DiscountEvaluator;


/**
 * Description of CouponFacade
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponFacade
{

	/**
	 * @var CouponRepository
	 */
	private $couponRepository;

	/**
	 * @var CouponValidator
	 */
	private $couponValidator;

	/**
	 * @var DiscountEvaluator
	 */
	private $discountEvaluator;

	/**
	 * @var EventManager
	 */
	private $evm;

	/**
	 * @var ICouponCart
	 */
	private $cart;

	/**
	 * @var Coupon
	 */
	private $coupon;


	public function __construct(CouponRepository $couponRepository, EventManager $evm)
	{
		$this->couponRepository = $couponRepository;
		$this->couponValidator = new CouponValidator($this, $couponRepository);
		$this->discountEvaluator = new DiscountEvaluator($couponRepository, $evm);
		$this->evm = $evm;
	}


	/**
	 * @return ICouponCart
	 */
	public function getCart()
	{
		return $this->cart;
	}


	/**
	 * @return Coupon
	 */
	public function getCoupon()
	{
		return $this->coupon;
	}


	public function load(ICouponCart $cart)
	{
		$this->cart = $cart;
		$this->coupon = $this->couponRepository->getCartCoupon($this->cart->getId());
		if (!$this->coupon->getId()) {
			return;
		}

		$result = $this->couponValidator->validate($this->cart, $this->coupon);
		$this->discountEvaluator->evaluate($this->coupon, $this->cart);
		if ($this->coupon->getTotalDiscountApplied() === 0) {
			$result = CouponValidator::NO_DISCOUNT;
		}
		if ($result !== CouponValidator::IS_OK) {
			$this->removeCoupon($this->coupon->getId(), $this->coupon->getItemId(), $result);
		} else {
			$this->evm->dispatchEvent('Redenge\Coupon\FrontModule\CouponFacade::onCouponAdd', new EventArgsList([$this->coupon]));

			
		}
	}


	/**
	 * 
	 * @param int $multishopId
	 * @param int $couponItemCode
	 * @return int
	 */
	public function addCoupon($multishopId, $couponItemCode)
	{
		$this->coupon = $this->couponRepository->getCouponByItemCode($multishopId, $couponItemCode);
		$result = $this->couponValidator->validate($this->cart, $this->coupon);
		if ($result === CouponValidator::IS_OK) {
			$this->discountEvaluator->evaluate($this->coupon, $this->cart);
			if ($this->coupon->getTotalDiscountApplied() > 0) {
				$this->couponRepository->addCoupon($this->cart->getId(), $this->coupon->getId(), $this->coupon->getItemId());
				$this->evm->dispatchEvent('Redenge\Coupon\FrontModule\CouponFacade::onCouponAdd', new EventArgsList([$this->coupon]));
			} else {
				$result = CouponValidator::NO_DISCOUNT;
			}
		}

		return $result;
	}


	public function removeCoupon($couponId, $couponItemId, $result = null)
	{
		$this->couponRepository->removeCoupon($this->cart->getId(), $couponId, $couponItemId);
		$this->coupon->setId(0);
		$this->coupon->setValidationResult($result);
		$this->evm->dispatchEvent('Redenge\Coupon\FrontModule\CouponFacade::onCouponRemove', new EventArgsList([$this->coupon]));
	}

}
