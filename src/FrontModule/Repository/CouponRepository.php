<?php

namespace Redenge\Coupon\FrontModule\Repository;

use Redenge\Coupon\FrontModule\Entity\Coupon;


/**
 * Description of CouponRepository
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponRepository extends AbstractRepository
{

	public function getCouponByItemCode($multishopId, $couponItemCode)
	{
		$select = 'coupon.*, :coupon_item.id itemId, :coupon_item.code itemCode, :coupon_item.used itemUsed, :coupon_item.max_use itemMaxUse'
			. ', IF((coupon.valid_from=\'0000-00-00\' OR coupon.valid_from<=NOW()) && (coupon.valid_to=\'0000-00-00\' OR coupon.valid_to>=NOW()), 1, 0) isDateValid ';
		$condition = [
			'coupon.multishop_id=?' => $multishopId,
			':coupon_item.code=?' => $couponItemCode,
		];

		$return = new Coupon;
		if ($coupon = $this->db->table('coupon')->select($select)->where($condition)->fetch()) {
			$return->loadFromObject($coupon);
			$return->setUsed($this->getCouponUsed($return->getId()));
		}

		return $return;

	}


	public function getCartCoupon($cartId)
	{
		$select = 'coupon.*, coupon_item.id itemId, coupon_item.code itemCode, coupon_item.used itemUsed, coupon_item.max_use itemMaxUse'
			. ', IF((coupon.valid_from=\'0000-00-00\' OR coupon.valid_from<=NOW()) && (coupon.valid_to=\'0000-00-00\' OR coupon.valid_to>=NOW()), 1, 0) isDateValid ';
		$condition = [
			'cart_coupon.cart_id=?' => $cartId,
		];

		$return = new Coupon;
		if ($coupon = $this->db->table('cart_coupon')->select($select)->where($condition)->fetch()) {
			$return->loadFromObject($coupon);
			$return->setUsed($this->getCouponUsed($return->getId()));
		}

		return $return;
	}


	public function getCouponUsed($couponId)
	{
		return $this->db->table('coupon_item')->select('SUM(coupon_item.used)')->where('coupon_id=?', $couponId)->fetchField();
	}


	/**
	 * 
	 * @param int $couponId
	 * @param int $validType
	 * @return array
	 */
	public function getCartProductForDiscount($cartId, $couponId, $validType)
	{
		switch ($validType) {
			case 1:
				$join = sprintf('JOIN coupon_product ON coupon_product.product_id = cart_product.id_product'
					. ' AND coupon_product.coupon_id = %d', $couponId);
				break;
			case 2:
				$join = sprintf('JOIN product_category ON product_category.id_product=cart_product.id_product'
					. ' JOIN coupon_category ON coupon_category.category_id=product_category.id_category'
					. ' AND coupon_category.coupon_id = %d', $couponId);
				break;
			case 3:
				$pairs = $this->db->table('coupon_attribute')->where('coupon_id=?', $couponId)->fetchPairs('name', 'value');
				if (!empty($pairs)) {
					$join = 'JOIN product ON product.id = cart_product.id_product';
					foreach ($pairs as $name => $value) {
						$join .= sprintf(" AND product.%s = %s", $name, is_numeric($value) ? $value : "'$value'");
					}
				}
				break;
			case 4:
				$join = sprintf('JOIN product_property ON product_property.id_product = cart_product.id_product'
					. ' JOIN coupon_property ON coupon_property.property_id = product_property.id_property'
					. ' AND coupon_property.coupon_id = %d'
					. ' AND (coupon_property.property_value_id = product_property.id_property_value)',
					$couponId);
				break;
			case 5:
				$join = sprintf('JOIN product_category ON product_category.id_product=cart_product.id_product'
					. ' JOIN coupon_category ON coupon_category.category_id=product_category.id_category'
					. ' AND coupon_category.coupon_id = %d', $couponId);

				$pairs = $this->db->table('coupon_attribute')->where('coupon_id=?', $couponId)->fetchPairs('name', 'value');
				if (!empty($pairs)) {
					$join .= ' JOIN product ON product.id = cart_product.id_product';
					foreach ($pairs as $name => $value) {
						$join .= sprintf(" AND product.%s = %s", $name, is_numeric($value) ? $value : "'$value'");
					}
				}
				break;
			case 6:
				$join = sprintf('JOIN product_category ON product_category.id_product=cart_product.id_product'
					. ' JOIN coupon_category ON coupon_category.category_id=product_category.id_category'
					. ' AND coupon_category.coupon_id = %d'
					. ' JOIN product_property ON product_property.id_product = cart_product.id_product'
					. ' JOIN coupon_property ON coupon_property.property_id = product_property.id_property'
					. ' AND coupon_property.coupon_id = %d'
					. ' AND (coupon_property.property_value_id = product_property.id_property_value)',
					$couponId, $couponId);
				break;
		}

		$query = 'SELECT cart_product.id_product FROM cart_product';
		if (!empty($join)) {
			$query .= ' ' . $join;
		}
		$query .= sprintf(' WHERE cart_product.id_cart = %d GROUP BY cart_product.id_product', $cartId);

		return $this->db->query($query)->fetchPairs('id_product', 'id_product');
	}


	public function addCoupon($cartId, $couponId, $couponItemId)
	{
		$this->db->table('cart_coupon')->insert([
			'cart_id' => $cartId,
			'coupon_id' => $couponId,
			'coupon_item_id' => $couponItemId
		]);
	}


	public function removeCoupon($cartId, $couponId, $couponItemId)
	{
		$this->db->table('cart_coupon')->where([
			'cart_id' => $cartId,
			'coupon_id' => $couponId,
			'coupon_item_id' => $couponItemId
		])->delete();
	}


	public function setUsed($couponItemId)
	{
		$used = $this->db->table('coupon_item')->where('id=?', $couponItemId)->max('used');
		$this->db->table('coupon_item')->where('id=?', $couponItemId)->update([
			'used' => $used + 1
		]);
	}


	public function appendToOrder($orderId, Coupon $coupon)
	{
		$this->db->table('order_coupon')->insert([
			'code' => $coupon->getItemCode(),
			'price' => $coupon->getTotalDiscountApplied(),
			'id_order' => $orderId
		]);
	}

}
