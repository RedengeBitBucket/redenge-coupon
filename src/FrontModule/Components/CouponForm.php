<?php

namespace Redenge\Coupon\FrontModule\Components;

use Exception;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Localization\ITranslator;
use Redenge\Coupon\FrontModule\Localization\SimpleTranslator;
use Redenge\Coupon\FrontModule\CouponFacade;
use Redenge\Coupon\FrontModule\CouponValidator;
use ShopModel;


/**
 * Description of CouponControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponForm extends Control
{

	/**
	 * @var int
	 */
	private $multishopId;

	/**
	 * @var CouponFacade
	 */
	private $couponFacade;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var string
	 */
	private $templateFile;


	public function __construct($multishopId, CouponFacade $couponFacade)
	{
		$this->multishopId = $multishopId;
		$this->couponFacade = $couponFacade;
	}


	/**
	 * @param ITranslator $translator
	 */
	public function setTranslator(ITranslator $translator)
	{
		$this->translator = $translator;
	}


	/**
	 * @param string $templateFile
	 */
	public function setTemplateFile($templateFile)
	{
		$this->templateFile = (string) $templateFile;
	}


	/**
	 * @return string
	 */
	public function getTemplateFile()
	{
		return $this->templateFile ?: __DIR__ . '/templates/couponForm.latte';
	}


	/**
	 * @return Form
	 */
	public function createComponentForm()
	{
		$form = new Form;
		if ($this->translator === null) {
			$this->setTranslator(new SimpleTranslator);
		}
		$form->setTranslator($this->translator);

		$form->addText('code', 'label_coupon')
			->setAttribute('class', 'form-control')
			->setAttribute('placeholder', 'redenge_coupon.enter_code')
			->setRequired('redenge_coupon.required');
		$form->addSubmit('save', 'redenge_coupon.save')
			->setAttribute('class', 'btn btn-info ac ajax');

		$form->onSuccess[] = [$this, 'formSuccess'];

		if ($this->couponFacade->getCoupon()->getId() > 0) {
			$form['code']->setDisabled();
			$form['save']->setDisabled();
		}

		return $form;
	}


	public function formSuccess(Form $form)
	{
		$couponItemCode = $form->values->code;

		$result = $this->couponFacade->addCoupon($this->multishopId, $couponItemCode);
		switch ($result) {
			case CouponValidator::IS_OK:
				$form['code']->setDisabled();
				$form['save']->setDisabled();
				$this->template->message = sprintf($this->translator->translate('redenge_coupon.add_success'), $this->couponFacade->getCoupon()->getName());

				$discountPercentMax = $this->couponFacade->getCoupon()->getDiscountPercentMax();
				if ($discountPercentMax > 0 && $discountPercentMax < 100) {
					$this->template->maxPercentInfo = sprintf($this->translator->translate('redenge_coupon.discount_percent_max_info'), $discountPercentMax);
				}
				break;
			case CouponValidator::NOT_EXISTS:
				$this->template->message = $this->translator->translate('redenge_coupon.not_exists');
				break;
			case CouponValidator::TOTAL_PRICE_IS_SMALLER_THAN_DISCOUNT:
				$this->template->message = $this->translator->translate('redenge_coupon.error_discount_is_bigger_than_total_price');
				break;
			case CouponValidator::TOTAL_PRICE_IS_SMALLER_THAN_PRICE_FROM:
				$this->template->message = sprintf($this->translator->translate('redenge_coupon.error_price_from'), $this->couponFacade->getCart()->getFormatPrice($this->couponFacade->getCoupon()->getPriceFrom()));
				break;
			case CouponValidator::MAX_USED:
				$this->template->message = $this->translator->translate('redenge_coupon.error_max_used');
				break;
			case CouponValidator::DATE_VALIDITY_EXPIRED:
				$this->template->message = $this->translator->translate('redenge_coupon.error_validity_expired');
				break;
			case CouponValidator::DISCOUNT_IS_BIGGER_THAN_PRODUCTS_PRICE:
				$this->template->message = $this->translator->translate('redenge_coupon.error_discount_is_bigger_than_products_price');
				break;
			case CouponValidator::NO_PRODUCTS_FOR_DISCOUNT:
				$this->template->message = $this->translator->translate('redenge_coupon.no_products_for_discount');
				break;
			case CouponValidator::NO_DISCOUNT:
				$this->template->message = $this->translator->translate('redenge_coupon.no_discount');
				break;
			default:
				$this->template->message = $this->translator->translate('redenge_coupon.not_exists');
		}

		$this->template->showModal = true;		

		$this->redrawControl('modal');
	}


	public function render()
	{
		if ($this->translator === null) {
			$this->template->setTranslator(new SimpleTranslator);
		} else {
			$this->template->setTranslator($this->translator);
		}
		$this->template->setFile($this->getTemplateFile());
		$this->template->render();
	}

}
