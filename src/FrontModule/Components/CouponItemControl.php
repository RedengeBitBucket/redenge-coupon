<?php

namespace Redenge\Coupon\FrontModule\Components;

use Nette\Application\UI\Control;
use Nette\Localization\ITranslator;
use Redenge\Coupon\FrontModule\CouponFacade;
use Redenge\Coupon\FrontModule\CouponValidator;
use Redenge\Coupon\FrontModule\Entity\Coupon;


/**
 * Description of CouponItemForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponItemControl extends Control
{

	/**
	 * @var Coupon
	 */
	private $coupon;

	/**
	 * @var CouponFacade
	 */
	private $couponFacade;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var string
	 */
	private $templateFile;

	/**
	 * @var array
	 */
	private $templateParameters;


	public function __construct(Coupon $coupon, CouponFacade $couponFacade)
	{
		$this->coupon = $coupon;
		$this->couponFacade = $couponFacade;
	}


	public function setTranslator(ITranslator $translator)
	{
		$this->translator = $translator;
	}


	public function setTemplateFile($templateFile)
	{
		$this->templateFile = (string) $templateFile;
	}


	public function setTemplateParameters(array $parameters)
	{
		$this->templateParameters = $parameters;
	}


	public function getTemplateFile()
	{
		return $this->templateFile ?: __DIR__ .'/templates/couponItemControl.latte';
	}


	public function handleDeleteCoupon($couponId, $couponItemId)
	{
		$this->couponFacade->removeCoupon($couponId, $couponItemId, CouponValidator::MANUAL_REMOVE);
	}


	public function render()
	{
		if (!empty($this->templateParameters)) {
			$this->template->setParameters($this->templateParameters);
		}
		$this->template->coupon = $this->coupon;
		$this->template->totalDiscount = $this->couponFacade->getCart()->getFormatPrice($this->coupon->getTotalDiscountApplied());
		$this->template->setFile($this->getTemplateFile());
		$this->template->setTranslator($this->translator);
		$this->template->render();
	}

}
