<?php

namespace Redenge\Coupon\FrontModule\Components;

use Redenge\Coupon\FrontModule\Entity\Coupon;


/**
 * Description of ICouponItemForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponItemControl
{

	/**
	 * @return CouponItemControl
	 */
	function create(Coupon $coupon);

}
