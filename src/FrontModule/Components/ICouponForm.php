<?php

namespace Redenge\Coupon\FrontModule\Components;


/**
 * Description of ICouponControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponForm
{

	/**
	 * @return CouponForm
	 */
	function create($multishopId);

}
