<?php

namespace Redenge\Coupon\FrontModule\DI;

use Nette\DI\CompilerExtension;


class CouponExtension extends CompilerExtension
{

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();

		$this->compiler->parseServices($builder, $this->loadFromFile(__DIR__ . '/../config/services.neon'), $this->name);
	}

}
