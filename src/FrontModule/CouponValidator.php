<?php

namespace Redenge\Coupon\FrontModule;

use Redenge\Coupon\FrontModule\Entity\Coupon;
use Redenge\Coupon\FrontModule\Entity\ICouponCart;
use Redenge\Coupon\FrontModule\Repository\CouponRepository;


/**
 * Description of CouponValidator
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponValidator
{

	/**
	 * Validace proběhla úspěšně
	 */
	const IS_OK = 0;

	/**
	 * Kupón neexistuje
	 */
	const NOT_EXISTS = 1;

	/**
	 * Cena produktů v košíku je menší než sleva kupónu 
	 */
	const TOTAL_PRICE_IS_SMALLER_THAN_DISCOUNT = 2;

	/**
	 * Cena produktů v košíku je menší než cena od u kupónu
	 */
	const TOTAL_PRICE_IS_SMALLER_THAN_PRICE_FROM = 3;

	/**
	 * Kupón přesáhl maximální počet použití
	 */
	const MAX_USED = 4;

	/**
	 * Vypršela platnost kupónu
	 */
	const DATE_VALIDITY_EXPIRED = 5;

	/**
	 * V košíku není žádný produkt, na který lze uplatnit kupón
	 */
	const NO_PRODUCTS_FOR_DISCOUNT = 6;

	/**
	 * Sleva je větší než cena produktu
	 */
	const DISCOUNT_IS_BIGGER_THAN_PRODUCTS_PRICE = 7;

	/**
	 * Kupón neobsahuje žádnou slevu na produkty v košíku
	 */
	const NO_DISCOUNT = 8;

	/**
	 * Kupón byl vymazán ručně
	 */
	const MANUAL_REMOVE = 9;

	/**
	 * @var CouponRepository
	 */
	private $couponRepository;

	/**
	 * @var CouponFacade
	 */
	private $couponFacade;


	public function __construct(CouponFacade $couponFacade, CouponRepository $couponRepository)
	{
		$this->couponRepository = $couponRepository;
		$this->couponFacade = $couponFacade;
	}


	/**
	 * @param Coupon $coupon
	 * @return int
	 */
	public function validate(ICouponCart $cart, Coupon $coupon)
	{
		if (empty($coupon->getId())) {			
			return self::NOT_EXISTS;
		}
		if ($coupon->isActive() === false) {
			return self::NOT_EXISTS;
		}
		if ($coupon->getItemMaxUse() !== 0 && $coupon->getItemUsed() >= $coupon->getItemMaxUse()) {
			return self::MAX_USED;
		}
		if ($coupon->getNumberOfUses() !== 0 && $coupon->getUsed() >= $coupon->getNumberOfUses()) {
			return self::MAX_USED;
		}
		if ($coupon->isDateValid() === false) {
			return self::DATE_VALIDITY_EXPIRED;
		}
		$productsForDiscount = $this->couponRepository->getCartProductForDiscount($cart->getId(), $coupon->getId(), $coupon->getValidtype());
		if (empty($productsForDiscount)) {
			return self::NO_PRODUCTS_FOR_DISCOUNT;
		}
		if ($coupon->getPriceFrom() > 0 && $cart->getTotalPrice() < $coupon->getPriceFrom()) {
			return self::TOTAL_PRICE_IS_SMALLER_THAN_PRICE_FROM;
		}
		if ($coupon->getDiscount() > 0 && $coupon->getDiscount() > $cart->getTotalPrice()) {
			return self::TOTAL_PRICE_IS_SMALLER_THAN_DISCOUNT;
		}
		$couponProducts = $this->couponFacade->getCart()->getCouponProducts($productsForDiscount);
		if ($coupon->getDiscount() > 0 && $coupon->getDiscount() > $this->getTotalPriceForDiscount($couponProducts)) {
			return self::DISCOUNT_IS_BIGGER_THAN_PRODUCTS_PRICE;
		}

		return self::IS_OK;
	}


	/**
	 * @param Entity\CartProduct[] $couponProducts
	 */
	private function getTotalPriceForDiscount($couponProducts)
	{
		$return = 0;
		foreach ($couponProducts as $couponProduct) {
			$return += $couponProduct->getPrice();
		}

		return $return;
	}

}
