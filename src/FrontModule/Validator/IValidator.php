<?php

namespace Redenge\Coupon\FrontModule\Validator;

use Redenge\Coupon\FrontModule\Entity\Coupon;


/**
 * Description of IValidator
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IValidator
{

	public function isValid(Coupon $coupon);

}
