<?php

namespace Redenge\Coupon\FrontModule;

use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Redenge\Coupon\FrontModule\Entity\Coupon;
use Redenge\Coupon\FrontModule\Entity\ICouponCart;
use Redenge\Coupon\FrontModule\Repository\CouponRepository;


/**
 * Description of DiscountEvaluator
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class DiscountEvaluator
{

	/**
	 * @var CouponRepository
	 */
	private $couponRepository;

	/**
	 * @var EventManager
	 */
	private $evm;

	/**
	 * @var ICouponCart
	 */
	private $cart;


	public function __construct(CouponRepository $couponRepository, EventManager $evm)
	{
		$this->couponRepository = $couponRepository;
		$this->evm = $evm;
	}


	/**
	 * Sory za tuhle sračku. Moc hodin, málo času
	 * Tak to dopadá, když se tlačí na dodání
	 *
	 * @param Coupon $coupon
	 * @param ICouponCart $cart
	 * @return type
	 */
	public function evaluate(Coupon &$coupon, ICouponCart $cart)
	{
		$productsForDiscount = $this->couponRepository->getCartProductForDiscount($cart->getId(), $coupon->getId(), $coupon->getValidtype());
		if (empty($productsForDiscount)) {
			return;
		}
		$couponProducts = $cart->getCouponProducts($productsForDiscount);

		if ($coupon->getDiscount() > 0) {
			$percentDiscount = 100 / ($this->getTotalPriceForDiscount($couponProducts) / $coupon->getDiscount());
			$discount = 0;
			foreach ($couponProducts as $couponProduct) {
				$newPrice = $this->getDiscount($couponProduct->getPrice(), $percentDiscount, $coupon->getDiscountPercentMax());
				$discount += (($couponProduct->getPrice() - $newPrice) * $couponProduct->getQuantity());
				$couponProduct->setPriceCoupon($newPrice);
			}
			$coupon->setTotalDiscountApplied($discount);
		} else {
			$discount = 0;
			if ($coupon->getDiscountPercent() > 0) {
				foreach ($couponProducts as $couponProduct) {
					if ($couponProduct->isPriceLevelDiscount() === false && $couponProduct->isPromotionDiscount() === false) {
						$newPrice = $this->getDiscount($couponProduct->getPrice(), $coupon->getDiscountPercent(), $coupon->getDiscountPercentMax());
						$discount += (($couponProduct->getPrice() - $newPrice) * $couponProduct->getQuantity());
						$couponProduct->setPriceCoupon($newPrice);
					}
				}
			}
			if ($coupon->getDiscountPercentPricelevel() > 0) {
				foreach ($couponProducts as $couponProduct) {
					if ($couponProduct->isPriceLevelDiscount() === true) {
						$newPrice = $this->getDiscount($couponProduct->getPrice(), $coupon->getDiscountPercentPricelevel(), $coupon->getDiscountPercentMax());
						$discount += (($couponProduct->getPrice() - $newPrice) * $couponProduct->getQuantity());
						$couponProduct->setPriceCoupon($newPrice);
					}
				}
			}
			if ($coupon->getDiscountPercentPromotion() > 0) {
				foreach ($couponProducts as $couponProduct) {
					if ($couponProduct->isPromotionDiscount() === true) {
						$newPrice = $this->getDiscount($couponProduct->getPrice(), $coupon->getDiscountPercentPromotion(), $coupon->getDiscountPercentMax());
						$discount += (($couponProduct->getPrice() - $newPrice) * $couponProduct->getQuantity());
						$couponProduct->setPriceCoupon($newPrice);
					}
				}
			}
			$coupon->setTotalDiscountApplied($discount);
		}

		if ($discount > 0) {
			$this->evm->dispatchEvent('Redenge\Coupon\FrontModule\DiscountEvaluator::onCouponDiscount', new EventArgsList([$coupon, $couponProducts]));
		}
	}


	/**
	 * @param Entity\CartProduct[] $couponProducts
	 */
	private function getTotalPriceForDiscount($couponProducts)
	{
		$return = 0;
		foreach ($couponProducts as $couponProduct) {
			$return += ($couponProduct->getPrice() * $couponProduct->getQuantity());
		}

		return $return;
	}


	/**
	 * 
	 * @param type $price
	 * @param type $discountPercent
	 * @param int $discountPercentMax Maximální sleva v % pro produkt
	 * @return type
	 */
	private function getDiscount($price, $discountPercent, $discountPercentMax)
	{
		$discount = $discountPercentMax > 0 && $discountPercentMax < 100 && $discountPercentMax < $discountPercent ? $discountPercentMax : $discountPercent;
		$newPrice = $price - ($price / 100 * $discount);

		return $newPrice;
	}

}
