SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `multishop_id` int(10) unsigned NOT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `validtype` tinyint(1) unsigned NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  `number_of_uses` smallint(5) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL,
  `price_from` decimal(8,2) unsigned NOT NULL,
  `discount` decimal(8,2) unsigned NOT NULL,
  `discount_percent` tinyint(1) unsigned NOT NULL,
  `discount_percent_pricelevel` tinyint(1) unsigned NOT NULL,
  `discount_percent_promotion` tinyint(1) unsigned NOT NULL,
  `discount_percent_max` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `multishop_id` (`multishop_id`),
  CONSTRAINT `coupon_ibfk_1` FOREIGN KEY (`multishop_id`) REFERENCES `multishop` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

DROP TABLE IF EXISTS `coupon_product`;
CREATE TABLE `coupon_product` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `coupon_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB';

DROP TABLE IF EXISTS `coupon_category`;
CREATE TABLE `coupon_category` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `coupon_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB';

DROP TABLE IF EXISTS `coupon_item`;
CREATE TABLE `coupon_item` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `coupon_id` int(10) unsigned NOT NULL,
  `used` smallint NOT NULL,
  `code` varchar(50) NOT NULL,
  FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB';

DROP TABLE IF EXISTS `coupon_property`;
CREATE TABLE `coupon_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `archetype_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `property_value_id` int(10) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `property_id` (`property_id`),
  KEY `archetype_id` (`archetype_id`),
  CONSTRAINT `coupon_property_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE,
  CONSTRAINT `coupon_property_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`),
  CONSTRAINT `coupon_property_ibfk_3` FOREIGN KEY (`archetype_id`) REFERENCES `archetype` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `coupon_attribute`;
CREATE TABLE `coupon_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_id` (`coupon_id`),
  CONSTRAINT `coupon_attribute_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `cart_coupon`;
CREATE TABLE `cart_coupon` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `cart_id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned NOT NULL,
  FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB';

SET foreign_key_checks = 1;

ALTER TABLE `coupon_item` ADD `max_use` smallint(6) NOT NULL AFTER `used`;

ALTER TABLE `cart_coupon`
ADD `coupon_item_id` int(10) unsigned NOT NULL,
ADD FOREIGN KEY (`coupon_item_id`) REFERENCES `coupon_item` (`id`) ON DELETE CASCADE;

DROP TABLE IF EXISTS `order_coupon`;
CREATE TABLE `order_coupon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8_bin NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_order` (`id_order`),
  CONSTRAINT `order_coupon_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
