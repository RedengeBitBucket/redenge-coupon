<?php

namespace Redenge\Coupon\Model;

use DBObject;


/**
 * Description of coupon
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 *
 * @property-read coupon_product        $product
 * @property-read coupon_category       $category
 * @property-read coupon_item           $item
 * @property-read coupon_property       $property
 * @property-read coupon_attribute      $attribute
 */
class coupon extends DBObject
{

	public function __construct($database, $parentNode = null)
	{
		parent::__construct($database, 'coupon', $parentNode);

		parent::createString('code');
		parent::createString('name');
		parent::createInteger('type');
		parent::createInteger('validtype');
		parent::createInteger('number_of_uses');
		parent::createInteger('multishop_id');
		parent::createDate('valid_from');
		parent::createDate('valid_to');
		parent::createBool('active');
		parent::createFloat('price_from');
		parent::createFloat('discount');
		parent::createInteger('discount_percent');
		parent::createInteger('discount_percent_pricelevel');
		parent::createInteger('discount_percent_promotion');
		parent::createInteger('discount_percent_max');
		parent::createObject('product', coupon_product::class);
		parent::createObject('category', coupon_category::class);
		parent::createObject('item', coupon_item::class);
		parent::createObject('property', coupon_property::class);
		parent::createObject('attribute', coupon_attribute::class);
	}

}
