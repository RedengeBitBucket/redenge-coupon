<?php

namespace Redenge\Coupon\Model;

use DBObject;


/**
 * Description of coupon
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class coupon_category extends DBObject
{

	public function __construct($database, $parentNode = null)
	{
		parent::__construct($database, 'coupon_category', $parentNode);

		parent::createInteger('coupon_id');
		parent::createInteger('category_id');
	}


	/**
	 * @param int   $couponId
	 * @param array $categoryIds
	 
	 *
	 * @return void
	 * @throws \Redenge\Exception\DBException
	 * @throws \Redenge\Exception\DuplicateKeyException
	 */
	public function insert($couponId, array $categoryIds)
	{
		if (empty($categoryIds)) {
			return;
		}

		$filter = sprintf('coupon_id = %d AND category_id IN (%s)', $couponId, implode(',', $categoryIds));
		$result = $this->getRecords('category_id', null, null, $filter);
		$existsCategoryIds = mysqli_fetch_all($result);
		if (!empty($existsCategoryIds)) {
			$existsCategoryIds = call_user_func_array('array_merge', $existsCategoryIds);
			$categoryIds = array_diff($categoryIds, $existsCategoryIds);
		}

		$sql = sprintf('INSERT INTO %s (coupon_id, category_id) VALUES %s', $this->table, implode(', ', array_map(function($categoryId) use ($couponId) {
			return sprintf("(%d, %d)", $couponId, $categoryId);
		}, $categoryIds)));

		$this->db->execSql($sql);
	}

	/**
	 * @param int   $couponId
	 * @param array $categoryIds
	 *
	 * @return void
	 * @throws \Redenge\Exception\DBException
	 * @throws \Redenge\Exception\DuplicateKeyException
	 */
	public function deleteNotIn($couponId, array $categoryIds)
	{
		$filter = ['coupon_id = ' . $couponId];
		if (!empty($categoryIds)) {
			$filter[] = sprintf("category_id NOT IN (%s)", implode(',', $categoryIds));
		}

		$sql = sprintf(
			"DELETE FROM %s WHERE %s",
			$this->table,
			implode(' AND ', $filter)
		);
		$this->db->execSql($sql);
	}

}
