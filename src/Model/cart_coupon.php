<?php

namespace Redenge\Coupon\Model;

use DBObject;


/**
 * Description of cart_coupon
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class cart_coupon extends DBObject
{

	public function __construct($database, $parentNode = null)
	{
		parent::__construct($database, 'cart_coupon', $parentNode);

		parent::createInteger('cart_id');
		parent::createInteger('coupon_id');
		parent::createInteger('coupon_item_id');
	}

}
