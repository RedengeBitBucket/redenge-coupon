<?php

namespace Redenge\Coupon\Model;

use DBObject;


/**
 * Description of coupon_property
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class coupon_property extends DBObject
{

	public function __construct($database, $parentNode = null)
	{
		parent::__construct($database, 'coupon_property', $parentNode);

		parent::createInteger('coupon_id');
		parent::createInteger('archetype_id');
		parent::createInteger('property_id');
		parent::createInteger('property_value_id');
		parent::createString('value');
	}


	/**
	 * @param int   $couponId
	 * @param array $propertyIds
	 
	 *
	 * @return void
	 * @throws \Redenge\Exception\DBException
	 * @throws \Redenge\Exception\DuplicateKeyException
	 */
	public function insert($couponId, array $propertyIds)
	{
		if (empty($propertyIds)) {
			return;
		}

		$filter = sprintf('coupon_id = %d AND property_id IN (%s)', $couponId, implode(',', $propertyIds));
		$result = $this->getRecords('property_id', null, null, $filter);
		$existsPropertyIds = mysqli_fetch_all($result);
		if (!empty($existsPropertyIds)) {
			$existsPropertyIds = call_user_func_array('array_merge', $existsPropertyIds);
			$propertyIds = array_diff($propertyIds, $existsPropertyIds);
		}

		$sql = sprintf('INSERT INTO %s (coupon_id, property_id) VALUES %s', $this->table, implode(', ', array_map(function($propertyId) use ($couponId) {
			return sprintf("(%d, %d)", $couponId, $propertyId);
		}, $propertyIds)));

		$this->db->execSql($sql);
	}

	/**
	 * @param int   $couponId
	 * @param int   $archetypeId
	 * @param array $propertyIds
	 *
	 * @return void
	 * @throws \Redenge\Exception\DBException
	 * @throws \Redenge\Exception\DuplicateKeyException
	 */
	public function deleteNotIn($couponId, $archetypeId, array $propertyIds)
	{
		$filter = ['coupon_id = ' . $couponId];
		if (!empty($propertyIds)) {
			$filter[] = sprintf("property_id NOT IN (%s)", implode(',', $propertyIds));
		}

		$sql = sprintf(
			"DELETE FROM %s WHERE %s",
			$this->table,
			implode(' AND ', $filter)
		);
		$this->db->execSql($sql);
	}


	/**
	 * @param int   $couponId
	 * @param int   $archetypeId
	 * @param int   $propertyId
	 * @param array $propertyValueIds
	 *
	 * @return void
	 * @throws \Redenge\Exception\DBException
	 * @throws \Redenge\Exception\DuplicateKeyException
	 */
	public function deleteValuesNotIn($couponId, $archetypeId, $propertyId, array $propertyValueIds)
	{
		$filter = ['coupon_id = ' . $couponId, 'archetype_id = ' . $archetypeId, 'property_id = ' . $propertyId];
		if (!empty($propertyValueIds)) {
			$filter[] = sprintf("property_value_id NOT IN (%s)", implode(',', $propertyValueIds));
		}

		$sql = sprintf(
			"DELETE FROM %s WHERE %s",
			$this->table,
			implode(' AND ', $filter)
		);
		$this->db->execSql($sql);
	}


	/**
	 * @param int $couponId
	 * @param int $propertyId
	 * @return array
	 */
	public function getPropertyValues($couponId, $propertyId, $archetypeId)
	{
		$archetypeId = $archetypeId !== null ? $archetypeId : 0;
		$return = [];
		$filter = sprintf('coupon_id = %d AND archetype_id = %d AND property_id = %d', $couponId, $archetypeId, $propertyId);
		$result = $this->getRecords('property_value_id', null, null, $filter);
		while ($row = mysqli_fetch_array($result)) {
			$return[] = $row[0];
		}

		return $return;
	}

}
