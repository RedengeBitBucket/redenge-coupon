<?php

namespace Redenge\Coupon\Model;

use DBObject;


/**
 * Description of coupon_item
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class coupon_item extends DBObject
{

	public function __construct($database, $parentNode = null)
	{
		parent::__construct($database, 'coupon_item', $parentNode);

		parent::createInteger('coupon_id');
		parent::createInteger('used');
		parent::createInteger('max_use');
		parent::createString('code');
	}


	public function setUsed($couponItemId)
	{
		$this->db->execSql('UPDATE `' . $this->table . '` SET used=used+1 WHERE id=' . intval($couponItemId));
	}

}
