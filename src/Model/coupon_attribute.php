<?php

namespace Redenge\Coupon\Model;

use DBObject;


/**
 * Description of coupon_property
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class coupon_attribute extends DBObject
{

	public function __construct($database, $parentNode = null)
	{
		parent::__construct($database, 'coupon_attribute', $parentNode);

		parent::createInteger('coupon_id');
		parent::createString('name');
		parent::createString('value');
	}

}
