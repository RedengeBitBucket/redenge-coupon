<?php

namespace Redenge\Coupon\Model;

use DBObject;


/**
 * Description of coupon
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class coupon_product extends DBObject
{

	public function __construct($database, $parentNode = null)
	{
		parent::__construct($database, 'coupon_product', $parentNode);

		parent::createInteger('coupon_id');
		parent::createInteger('product_id');
	}

}
