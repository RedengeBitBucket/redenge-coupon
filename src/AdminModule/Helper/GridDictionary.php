<?php

namespace Redenge\Coupon\AdminModule\Helper;


class GridDictionary
{
	/**
	 * @return array
	 */
	public static function cs()
	{
		return [
			'ublaboo_datagrid.no_item_found_reset' => 'Žádné záznamy, můžete resetovat filtry.',
			'ublaboo_datagrid.no_item_found' => 'Žádné záznamy',
			'ublaboo_datagrid.here' => 'zde',
			'ublaboo_datagrid.items' => 'Položky',
			'ublaboo_datagrid.all' => 'vše',
			'ublaboo_datagrid.from' => 'z',
			'ublaboo_datagrid.reset_filter' => 'Resetovat filtry',
			'ublaboo_datagrid.group_actions' => 'Hromadné akce',
			'ublaboo_datagrid.show' => 'Zobrazit',
			'ublaboo_datagrid.add' => 'Přidat',
			'ublaboo_datagrid.edit' => 'Editovat',
			'ublaboo_datagrid.show_all_columns' => 'Zobrazit všechny sloupce',
			'ublaboo_datagrid.show_default_columns' => 'Zobrazit výchozí sloupce',
			'ublaboo_datagrid.hide_column' => 'Schovat sloupec',
			'ublaboo_datagrid.action' => 'Akce',
			'ublaboo_datagrid.previous' => 'Předchozí',
			'ublaboo_datagrid.next' => 'Další',
			'ublaboo_datagrid.choose' => 'Vyberte',
			'ublaboo_datagrid.choose_input_required' => 'Group action text not allow empty value',
			'ublaboo_datagrid.execute' => 'Provést',
			'ublaboo_datagrid.save' => 'Uložit',
			'ublaboo_datagrid.cancel' => 'Zrušit',
			'ublaboo_datagrid.multiselect_choose' => 'Vyberte',
			'ublaboo_datagrid.multiselect_selected' => '{0} vybráno',
			'ublaboo_datagrid.filter_submit_button' => 'Filtrovat'
		];
	}
}
