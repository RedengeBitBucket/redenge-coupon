<?php

namespace Redenge\Coupon\AdminModule\Helper;

use Nette\Utils\Callback;
use Ublaboo\DataGrid\DataSource\NetteDatabaseTableDataSource as OriginalDataSource;
use Ublaboo\DataGrid\Filter;


class GridOwnDataSource extends OriginalDataSource
{

	/**
	 * Filter by keyword
	 * @param  Filter\FilterText $filter
	 * @return void
	 */
	public function applyFilterText(Filter\FilterText $filter)
	{
		$or = [];
		$args = [];
		$big_or = '(';
		$big_or_args = [];
		$condition = $filter->getCondition();

		foreach ($condition as $column => $value) {
			$like = '(';
			$args = [];

			if(!$filter->isExactSearch()){
				if (@preg_match('~' . $value . '~', '', $matches) === false){
					# Zatím nefunguje - volá se až v renderi, chce to kontrolovat dříve
					#Callback::invokeArgs($this->regexpFallback, [$value]);
					continue;
				}
				else{
					$like .= "$column REGEXP ? OR ";
					$args[] = $value;
				}
			}
			else{
				$like .=  "$column = ? OR ";
				$args[] = "$value";
			}
			$like = substr($like, 0, strlen($like) - 4) . ')';

			$or[] = $like;
			$big_or .= "$like OR ";
			$big_or_args = array_merge($big_or_args, $args);
		}

		if (sizeof($or) > 1) {
			$big_or = substr($big_or, 0, strlen($big_or) - 4).')';

			$query = array_merge([$big_or], $big_or_args);

			call_user_func_array([$this->data_source, 'where'], $query);
		} else {
			$query = array_merge($or, $args);
			if (!empty($query))
				call_user_func_array([$this->data_source, 'where'], $query);
		}
	}

}