<?php

namespace Redenge\Coupon\AdminModule\Presenters;

use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Nette\Forms\Container;
use Nette\Database\Context;
use Nette\Security\User;
use Nette\Utils\Strings;
use Redenge\Coupon\AdminModule\Components\ICategoryTreeForm;
use Redenge\Coupon\AdminModule\Components\ICouponAttributesForm;
use Redenge\Coupon\AdminModule\Components\ICouponItemControl;
use Redenge\Coupon\AdminModule\Helper\GridDictionary;
use Redenge\Coupon\AdminModule\Helper\GridOwnDataSource;
use Redenge\Engine\Presenters\BasePresenter;
use Redenge\Engine\Components\Tab\Bookmark;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Localization\SimpleTranslator;


/**
 * Description of CouponPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponPresenter extends BasePresenter
{

	/**
	 * @var Context @inject
	 */
	public $db;

	/**
	 * @var EventManager @inject
	 */
	public $evm;

	/**
	 * @var \Redenge\Coupon\AdminModule\Components\ICouponBaseForm @inject
	 */
	public $couponBaseForm;

	/**
	 * @var \Redenge\Coupon\AdminModule\Components\ICouponSettingsForm @inject
	 */
	public $couponSettingsForm;

	/**
	 * @var ICategoryTreeForm @inject
	 */
	public $categoryForm;

	/**
	 * @var ICouponAttributesForm @inject
	 */
	public $couponAttributesForm;

	/**
	 * @var ICouponItemControl @inject
	 */
	public $couponItemControl;

	/**
	 * @var \Redenge\Coupon\AdminModule\Components\ICouponPropertiesControl @inject
	 */
	public $couponPropertiesControl;

	/**
	 * @var User @inject
	 */
	public $user;


	public function startup()
	{
		if (!$this->user->isLoggedIn()) {
			$this->presenter->redirectUrl('/admin');
		}

		parent::startup();
	}


	/**
	 * @param int $id
	 */
	public function actionEditCoupon($id)
	{
		if (!$this->db->table('coupon')->where('id', $id)->fetch()) {
			$this->flashMessage('Položka neexistuje.', 'danger');
			$this->redirect('Coupon:');
		} else {
			$this['baseForm']->setId($id);
			$this['settingsForm']->setId($id);
		}
	}


	/**
	 * @param int $id
	 */
	public function actionEditCouponItem($id)
	{
		if (!$this->db->table('coupon_item')->where('id', $id)->fetch()) {
			$this->flashMessage('Položka neexistuje.', 'danger');
			$this->redirect('Coupon:');
		} else {
			//$this['baseForm']->setId($id);
			//$this['settingsForm']->setId($id);
		}
	}


	/**
	 * @param int $couponId
	 */
	public function handleDeleteCoupon($couponId)
	{
		if (!$this->db->table('coupon')->get($couponId)) {
			$this->flashMessage('Položka neexistuje.', 'danger');
		} else {
			$this->db->table('coupon')->wherePrimary($couponId)->delete();
			$this->flashMessage('Produkt byl odstraněn.', 'success');
			$this['couponGrid']->reload();
		}
		$this->redrawControl('flashes');
	}


	/**
	 * @param int $productId
	 */
	public function handleDeleteProduct($productId)
	{
		$this->deleteProducts([$productId]);
	}


	/**
	 * @return \Redenge\Admin\Components\Tab\TabControl
	 */
	protected function createComponentMainTab()
	{
		$control = $this->tabControl->create('Kupóny');

		$main = $control->getTab();

		# Main
		$main->addBookmark('Přehled', function (Bookmark $bookmark) {
			# if is action 'default' we shows grid
			if (!in_array($this->action, ['addCoupon', 'editCoupon'])) {
				return $this['couponGrid'];
			}

			$tab = $bookmark->addTab();
			$tab->addBookmark('Základní', function () { return $this['baseForm']; })
				->setAction($this->action == 'editCoupon' ? 'this' : 'Coupon:addCoupon');

			# if is action 'edit' we shows other bookmarks
			if ($this->action === 'editCoupon') {
				$tab->addBookmark('Nastavení', function () { return $this['settingsForm']; });
				$tab->addBookmark('Produkty', function () { return $this['productGrid']; });
				$tab->addBookmark('Kategorie', function () { return $this["categoryForm"]; });
				$tab->addBookmark('Vlastnosti', function () { return $this["propertiesControl"]; });
				$tab->addBookmark('Atributy', function () { return $this["attributesForm"]; });
				$tab->addBookmark('Kódy kupónu', function () { return $this['couponItemControl']; });
			}

			return $tab;

		})->setAction('Coupon:default');

		$main->addBookmark('Report', function (Bookmark $bookmark) {
		})->setAction('Report:default');

		return $control;
	}


	/**
	 * @return \Ublaboo\DataGrid\DataGrid
	 * @throws \Ublaboo\DataGrid\Exception\DataGridException
	 */
	protected function createComponentCouponGrid()
	{
		$grid = new DataGrid();

		$grid->setTranslator(new SimpleTranslator(GridDictionary::cs()));
		$grid->setDataSource(
			$this->db->table('coupon')
				->select('coupon.id, coupon.code, coupon.name')
			, $grid->getPrimaryKey()
		);
		$grid->setItemsPerPageList([20, 50, 100, 200, 500]);
		$grid->setColumnsHideable();

		$grid->addColumnText('code', 'Kód')
			->setSortable();
		$grid->addColumnDateTime('name', 'Název')
			->setSortable();

		$grid->addFilterText('code', 'Kód', 'coupon.code');
		$grid->addFilterText('name', 'Název', 'coupon.name');

		$grid->addAction('edit', '', 'Coupon:editCoupon')
			->setIcon('pencil')
			->setTitle('editovat')
			->setClass('btn btn-xs btn-default');

		$grid->addAction('deleteCoupon!', '', null, ['couponId' => $grid->getPrimaryKey()])
			->setConfirm('Opravdu chcete odstranit kupón "%s"', 'name')
			->setIcon('times')
			->setTitle('odstranit')
			->setClass('btn btn-xs btn-danger ajax');

		$grid->addToolbarButton('Coupon:addCoupon', ' Nový kupón')->setIcon('plus')->setClass('btn btn-success btn-xs');

		return $grid;
	}


	/**
	 * @return \Ublaboo\DataGrid\DataGrid
	 * @throws \Ublaboo\DataGrid\Exception\DataGridColumnStatusException
	 * @throws \Ublaboo\DataGrid\Exception\DataGridException
	 */
	protected function createComponentProductGrid()
	{
		$grid = new DataGrid();

		$grid->setTranslator(new SimpleTranslator(GridDictionary::cs()));
		$grid->setDataSource(new GridOwnDataSource(
			$this->db->table('product')
				->select('product.id, product.code, :product_attribute.name, IF(:coupon_product.coupon_id=?, 0, 1) active', $this->getParameter('id'))
				->where([
					':product_attribute.id_language' => 1
				])
			, $grid->getPrimaryKey()
		));
		$grid->setItemsPerPageList([20, 50, 100, 200, 500]);
		$grid->setColumnsHideable();

		$this->evm->dispatchEvent('Redenge\Coupon\AdminModule\Presenters\CouponPresenter::onBeforeCreateColumnProductGrid', new EventArgsList([$grid, $this->getParameter('id')]));

		$grid->addColumnText('code', 'Kód')
			->setSortable();
		$grid->addColumnText('name', 'Název')
			->setSortable();

		$grid->addColumnStatus('active', 'Aktivní')
			->setSortable()
			->setCaret(FALSE)
			->addOption(1, 'Ano')->setIcon('check')->setClass('btn-success')->endOption()
			->addOption(0, 'Ne')->setIcon('close')->setClass('btn-danger')->endOption()
			->onChange[] = [$this, 'changeActive'];

		$grid->addFilterText('code', 'Kód', 'product.code');
		$grid->addFilterText('name', 'Název', ':product_attribute.name')
			->setCondition(function($fluent, $value) {
				$fluent->where('LOWER(:product_attribute.name) REGEXP ?', Strings::lower($value));
			});;
		$grid->addFilterSelect('active', 'Aktivní', ['' => 'Vše', 1 => 'Ano', 0 => 'Ne'], 'IF(:coupon_product.coupon_id=' . $this->getParameter('id') . ', 1, 0)');

		# Delete product
		$grid->addAction('deleteProduct!', '', null, ['productId' => $grid->getPrimaryKey()])
			->setIcon('times')
			->setTitle('odstranit')
			->setClass('btn btn-xs btn-danger ajax');

		$grid->addGroupAction('Přidat')
			->onSelect[] = [$this, 'addProducts'];
		$grid->addGroupAction('Odstranit')
			->onSelect[] = [$this, 'deleteProducts'];

		$this->evm->dispatchEvent('Redenge\Coupon\AdminModule\Presenters\CouponPresenter::onAfterCreateColumnProductGrid', new EventArgsList([$grid, $this->getParameter('id')]));

		return $grid;
	}


	protected function createComponentCouponItemControl()
	{
		$control = $this->couponItemControl->create($this->getParameter('id'));

		return $control;
	}


	protected function createComponentBaseForm()
	{
		$form = $this->couponBaseForm->create();

		$form->onSuccess = function ($id, $edit) {
			if ($edit) $this->redirect('this');
			else $this->redirect('Coupon:editCoupon', $id);
		};

		$form->onError = function ($edit) {
			$this->flashMessage('Během ukládání došlo k chybě.','danger');
			$this->redirect('this');
		};

		return $form;
	}


	protected function createComponentSettingsForm()
	{
		$form = $this->couponSettingsForm->create();

		$form->onSuccess = function ($id) {
			$this->redirect('this', $id);
		};

		$form->onError = function () {
			$this->flashMessage('Během ukládání došlo k chybě.','danger');
			$this->redirect('this');
		};

		return $form;
	}


	/**
	 * @return \Nette\Application\UI\Multiplier
	 */
	protected function createComponentCategoryForm()
	{
		$form = $this->categoryForm->create($this->getParameter('id'));
		$form->onSuccess = function () {
			$this->flashMessage('Uloženo', 'success');
			$this->redirect('this');
		};
		$form->onError = function () {
			$this->flashMessage('Při ukládání došlo k chybě', 'danger');
			$this->redirect('this');
		};

		return $form;
	}


	protected function createComponentCouponGeneratorForm()
	{
		$form = $this->couponGeneratorForm->create($this->getParameter('id'));
		$form->onSuccess = function () {
			$this->flashMessage('Uloženo', 'success');
			$this->redirect('this');
		};
		$form->onError = function () {
			$this->flashMessage('Při ukládání došlo k chybě', 'danger');
			$this->redirect('this');
		};

		return $form;
	}


	protected function createComponentPropertiesControl()
	{
		$control = $this->couponPropertiesControl->create();

		/*$form->onSuccess = function ($id, $edit) {
			if ($edit) $this->redirect('this');
			else $this->redirect('Coupon:editCoupon', $id);
		};

		$form->onError = function ($edit) {
			$this->flashMessage('Během ukládání došlo k chybě.','danger');
			$this->redirect('this');
		};*/

		return $control;
	}


	protected function createComponentAttributesForm()
	{
		$form = $this->couponAttributesForm->create($this->getParameter('id'));

		return $form;
	}


	/**
	 * @param int $id
	 * @param int $newStatus
	 *
	 * @return void
	 */
	public function changeActive($productId, $newStatus)
	{
		$conditions = ['product_id' => $productId, 'coupon_id' => $this->getParameter('id')];
		if ((int)$newStatus === 1) {
			if ($this->db->table('coupon_product')->where($conditions)->count() === 0) {
				$this->db->query('INSERT INTO coupon_product', [
					'product_id' => $productId,
					'coupon_id' => $this->getParameter('id'),
				]);
			}
		} else {
			$this->db->table('coupon_product')->where($conditions)->delete();
		}
		if ($this->isAjax()) {
			$this['productGrid']->redrawItem($productId, 'product.id');
		}
	}


	/**
	 * @param array $ids
	 *
	 * @return void
	 */
	public function deleteProducts(array $ids)
	{
		$this->db->table('coupon_product')->where(['coupon_id' => $this->getParameter('id'), 'product_id' => $ids])->delete();
		$this->flashMessage('Produkty byly odstraněny', 'success');
		$this['productGrid']->reload();
		$this->redrawControl('flashes');
	}


	public function addProducts(array $ids)
	{
		$insertedCount = 0;
		foreach ($ids as $productId) {
			$conditions = ['product_id' => $productId, 'coupon_id' => $this->getParameter('id')];
			if ($this->db->table('coupon_product')->where($conditions)->count() === 0) {
				$this->db->query('INSERT INTO coupon_product', [
					'product_id' => $productId,
					'coupon_id' => $this->getParameter('id'),
				]);
				$insertedCount++;
			}
		}

		$this->flashMessage(sprintf('Přidáno %d produktů', $insertedCount), 'success');
		$this['productGrid']->reload();
		$this->redrawControl('flashes');
	}

}
