<?php

namespace Redenge\Coupon\AdminModule\Presenters;

use Nette\Forms\Container;
use Nette\Database\Context;
use Nette\Security\User;
use Redenge\Coupon\AdminModule\Components\ICategoryTreeForm;
use Redenge\Coupon\AdminModule\Helper\GridDictionary;
use Redenge\Engine\Presenters\BasePresenter;
use Redenge\Engine\Components\Tab\Bookmark;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\DataSource\NetteDatabaseTableDataSource;
use Ublaboo\DataGrid\Localization\SimpleTranslator;


/**
 * Description of ReportPresenter
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class ReportPresenter extends BasePresenter
{

	/**
	 * @var Context @inject
	 */
	public $db;

	/**
	 * @var int
	 */
	private $couponId;

	/**
	 * @var User @inject
	 */
	public $user;


	public function startup()
	{
		if (!$this->user->isLoggedIn()) {
			$this->presenter->redirectUrl('/admin');
		}

		parent::startup();
	}


	/**
	 * @return \Redenge\Admin\Components\Tab\TabControl
	 */
	protected function createComponentMainTab()
	{
		$control = $this->tabControl->create('Kupóny');

		$main = $control->getTab();

		# Main
		$main->addBookmark('Přehled')->setAction('Coupon:default');

		$main->addBookmark('Report', function (Bookmark $bookmark) {
			# if is action 'default' we shows grid
			if (!in_array($this->action, ['editReport'])) {
				return $this['reportGrid'];
			}

			if ($this->action === 'editReport') {
				return $this['editReportGrid'];
			}
		})->setAction('Report:default')->setActive();

		return $control;
	}


	/**
	 * @return \Ublaboo\DataGrid\DataGrid
	 * @throws \Ublaboo\DataGrid\Exception\DataGridColumnStatusException
	 * @throws \Ublaboo\DataGrid\Exception\DataGridException
	 */
	protected function createComponentReportGrid()
	{
		$grid = new DataGrid();

		$grid->setTranslator(new SimpleTranslator(GridDictionary::cs()));
		$grid->setDataSource(new NetteDatabaseTableDataSource(
			$this->db->table('coupon')
				->select('coupon.id, coupon.code, SUM(:coupon_item.used) used, multishop.code multishopCode')
				->group('coupon.id')
			, $grid->getPrimaryKey()));
		$grid->setItemsPerPageList([20, 50, 100, 200, 500]);
		$grid->setColumnsHideable();

		$grid->addColumnText('code', 'Kód')
			->setSortable();
		$grid->addColumnText('used', 'Počet uplatnění')
			->setSortable();
		$grid->addColumnText('multishopCode', 'Multishop')
			->setSortable();

		$grid->addFilterText('code', 'Kód', 'coupon.code');
		$grid->addFilterText('used', 'Počet uplatnění');
		$grid->addFilterText('multishopCode', 'Multishop');

		$grid->addAction('edit', '', 'Report:editReport')
			->setIcon('pencil')
			->setTitle('editovat')
			->setClass('btn btn-xs btn-default');

		//$grid->addToolbarButton('Report:exportCsv', 'Export CSV')->setIcon('floppy-o')->setClass('btn btn-success btn-xs');

		$grid->addExportCsv('Export CSV', 'Kupony-' . date('H_i-d_m_Y') . '.csv')
			->setTitle('Export CSV')
			->setIcon('floppy-o');

		return $grid;
	}


	public function createComponentEditReportGrid()
	{
		$grid = new DataGrid();

		$grid->setTranslator(new SimpleTranslator(GridDictionary::cs()));
		$grid->setDataSource(new NetteDatabaseTableDataSource(
			$this->db->table('order_coupon')
				->select('order_coupon.id, order_coupon.code, order.index, order.insert_date, order:order_customer.customer.login')
				->where('coupon_item.coupon.id=?', $this->couponId)
			, $grid->getPrimaryKey()));
		$grid->setItemsPerPageList([20, 50, 100, 200, 500]);
		$grid->setColumnsHideable();

		$grid->addColumnText('code', 'Kód')
			->setSortable();
		$grid->addColumnText('index', 'Číslo objednávky')
			->setSortable();
		$grid->addColumnDateTime('insert_date', 'Datum uplatnění')
			->setSortable();
		$grid->addColumnText('login', 'Login zákazníka')
			->setSortable()
			->setRenderer(function($item) {
				return $item->login ?: 'Anonymní zákazník';
			});

		$grid->addFilterText('code', 'Kód', 'order_coupon.code');
		$grid->addFilterText('index', 'Číslo objednávky', 'order.index');
		$grid->addFilterDate('insert_date', 'Datum uplatnění', 'order.insert_date');
		$grid->addFilterText('login', 'Login zákazníka', 'order:order_customer.customer.login');

		//$grid->addToolbarButton('Report:exportCsv', 'Export CSV')->setIcon('floppy-o')->setClass('btn btn-success btn-xs');

		$grid->addExportCsv('Export CSV', 'Kupony-' . date('H_i-d_m_Y') . '.csv')
			->setTitle('Export CSV')
			->setIcon('floppy-o');

		return $grid;
	}


	public function actionEditReport($id)
	{
		$this->couponId = (int) $id;
	}


	/**
	 * Not used. We using datagrid functions for export csv
	 */
	public function actionExportCsv()
	{
		$filename = 'reporty' . date('H_i-d_m_Y') . '.csv';
		$httpResponse = $this->getHttpResponse();
		$httpResponse->setContentType('text/csv', 'UTF-8');
		$httpResponse->setHeader('Content-Disposition', "attachment; filename=$filename");

		$output = fopen('php://output', 'w');
		fputcsv($output, array('Test', 'test'));
		fputcsv($output, array('Test2', 'test2'));
		fclose($output);
		
		exit;
	}
}
