<?php

namespace Redenge\Coupon\AdminModule\Components;

use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextBase;
use Nette\Utils\ArrayHash;
use Redenge\Engine\Components\BaseControl;
use ShopModel;


/**
 * Description of CouponAttributesControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponAttributesForm extends BaseControl
{

	/**
	 * @var int
	 */
	private $couponId;

	/**
	 * @var EventManager
	 */
	private $evm;

	/**
	 * @var ShopModel
	 */
	private $model;


	public function __construct($couponId, EventManager $evm, ShopModel $model)
	{
		$this->couponId = $couponId;
		$this->evm = $evm;
		$this->model = $model;
	}


	public function createComponentForm()
	{
		$form = new Form;

		$this->evm->dispatchEvent('Redenge\Coupon\AdminModule\Components\CouponAttributesForm::onCreateForm', new EventArgsList([$form]));

		$form->addSubmit('save', 'Uložit')->controlPrototype->class[] = 'btn btn-sm btn-success';
		$form->addButton('back', 'Zpět na výpis')
			->setAttribute('onclick', sprintf('window.location.href="%s"', $this->presenter->link('default')))
			->setAttribute('class', 'btn btn-sm btn-primary');

		$form->onSuccess[] = [$this, 'save'];

		foreach ($form->getControls() as $control) {
			if ($control instanceof TextBase || $control instanceof SelectBox) {
				$control->controlPrototype->class[] = 'form-control';
			}
		}

		$defaults = $this->getDefaults($this->couponId);
		foreach ($defaults as $name => $value) {
			if (!isset($form[$name])) {
				$this->model->coupon->attribute->load([
					'coupon_id' => $this->couponId,
					'name' => $name,
				]);
				$this->model->coupon->attribute->delete();
				continue;
			}
			if ($form[$name] instanceof SelectBox) {
				if (!array_key_exists($value, $form[$name]->getItems())) {
					$this->model->coupon->attribute->load([
						'coupon_id' => $this->couponId,
						'name' => $name,
					]);
					$this->model->coupon->attribute->delete();
				} else {
					$form[$name]->setValue($value);
				}
			} else {
				$form[$name]->setValue($value);
			}
		}

		return $form;
	}


	public function save(Form $form)
	{
		foreach ($form->values as $name => $value) {
			if ($name === 'back') {
				continue;
			}
			
			$this->model->coupon->attribute->load([
				'coupon_id' => $this->couponId,
				'name' => $name
			]);
			if ($value === null && $this->model->coupon->attribute->id > 0) {
				$this->model->coupon->attribute->delete();
			}
			if ($value === null) {
				continue;
			}
			$this->model->coupon->attribute->coupon_id = $this->couponId;
			$this->model->coupon->attribute->name = $name;
			$this->model->coupon->attribute->value = $value;
			$this->model->coupon->attribute->save();
		}
		$this->flashMessage('Uloženo', 'success');
		$this->redrawControl('flashes');
	}


	/**
	 * @param null|int $couponId
	 *
	 * @return array
	 */
	private function getDefaults($couponId)
	{
		if (!$couponId) {
			return [];
		}

		$defaults = [];
		$result = $this->model->coupon->attribute->getRecords(null, null, null, 'coupon_id=' . $couponId);
		while ($row = mysqli_fetch_assoc($result)) {
			$defaults[$row['name']] = $row['value'];
		}
		mysqli_free_result($result);

		return $defaults;
	}

}
