<?php

namespace Redenge\Coupon\AdminModule\Components;

/**
 * Description of ICouponAttributesForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponAttributesForm
{

	/**
	 * @return CouponAttributesForm
	 */
	function create($couponId);

}
