<?php

namespace Redenge\Coupon\AdminModule\Components;

use Exception;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextBase;
use Nette\Utils\DateTime;
use Redenge\Engine\Components\BaseControl;
use ShopModel;
use Tracy\Debugger;
use Tracy\ILogger;


/**
 * Description of CouponSettingsForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponSettingsForm extends BaseControl
{

	/**
	 * @var null|int
	 */
	private $id;

	/**
	 * @var ShopModel
	 */
	private $model;

	/**
	 * @var Context
	 */
	private $db;


	/**
	 * @var null|callable
	 */
	public $onSuccess;

	/**
	 * @var null|callable
	 */
	public $onError;


	/**
	 * @param ShopModel $model
	 */
	public function __construct(ShopModel $model, Context $db)
	{
		$this->model = $model;
		$this->db = $db;
	}


	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		$form = new Form();

		$form->addText('discount', 'Hodnotová sleva')
			->setValue(0)
			->addRule(Form::INTEGER);

		$form->addText('discount_percent', 'Procentuální sleva na nezlevněné zboží')
			->setValue(0)
			->addRule(Form::INTEGER);

		$form->addText('discount_percent_pricelevel', 'Procentuální sleva na zlevněné zboží z cenových úrovní')
			->setValue(0)
			->addRule(Form::INTEGER);

		$form->addText('discount_percent_promotion', 'Procentuální sleva na zlevněné zboží z promoce')
			->setValue(0)
			->addRule(Form::INTEGER);

		$form->addText('discount_percent_max', 'Maximální procentuální sleva na produktu')
			->setValue(0)
			->addRule(Form::INTEGER);

		$form->addText('price_from', 'Cena produktů od')
			->setValue(0)
			->addRule(Form::INTEGER);

		$form->addSubmit('save', 'Uložit')->controlPrototype->class[] = 'btn btn-sm btn-success';
		$form->addButton('back', 'Zpět na výpis')
			->setAttribute('onclick', sprintf('window.location.href="%s"', $this->presenter->link('default')))
			->setAttribute('class', 'btn btn-sm btn-primary');

		$form->onSuccess[] = [$this, 'save'];

		foreach ($form->getControls() as $control)
			if ($control instanceof TextBase || $control instanceof SelectBox)
				$control->controlPrototype->class[] = 'form-control';
				
		$form->setDefaults($this->getDefaults($this->id));

		return $form;
	}


	/**
	 * @param null|int $id
	 *
	 * @return array
	 */
	private function getDefaults($id)
	{
		$coupon = $this->model->coupon;
		$defaults = [];

		if (!$id || !$coupon->load(['id' => $id])->id) {
			return [];
		}

		$defaults['discount'] = $coupon->discount;
		$defaults['discount_percent'] = $coupon->discount_percent;
		$defaults['discount_percent_pricelevel'] = $coupon->discount_percent_pricelevel;
		$defaults['discount_percent_promotion'] = $coupon->discount_percent_promotion;
		$defaults['discount_percent_max'] = $coupon->discount_percent_max;
		$defaults['price_from'] = $coupon->price_from;

		return $defaults;
	}


	public function save(Form $form)
	{
		$coupon = $this->model->coupon;
		$edit = (boolean) $coupon->id;

		try {
			$values = $form->getValues();

			$coupon->discount = $values->discount;
			$coupon->discount_percent = $values->discount_percent;
			$coupon->discount_percent_pricelevel = $values->discount_percent_pricelevel;
			$coupon->discount_percent_promotion = $values->discount_percent_promotion;
			$coupon->discount_percent_max = $values->discount_percent_max;
			$coupon->price_from = $values->price_from;
			$coupon->save();

			if (is_callable($this->onSuccess)) {
				$this->onSuccess($coupon->id, $edit);
			}
		} catch (AbortException $e) {
			throw $e;
		} catch (Exception $e) {
			Debugger::log($e->getMessage(), ILogger::EXCEPTION);
			if (is_callable($this->onError)) {
				$this->onError($edit);
			}
		}
	}

}
