<?php

namespace Redenge\Coupon\AdminModule\Components;

/**
 * Description of ICouponGeneratorForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponGeneratorForm
{

	/**
	 * @return CouponGeneratorForm
	 */
	function create($couponId);

}
