<?php

namespace Redenge\Coupon\AdminModule\Components;

use Nette\Database\Context;


/**
 * Description of ICouponPropertiesControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponPropertiesControl
{

	/**
	 * @return CouponPropertiesControl
	 */
	function create();

}
