<?php

namespace Redenge\Coupon\AdminModule\Components;

/**
 * Description of ICouponItemControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponItemControl
{

	/**
	 * @param type $couponId
	 * @return CouponItemControl
	 */
	function create($couponId);

}
