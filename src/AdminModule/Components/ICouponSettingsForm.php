<?php

namespace Redenge\Coupon\AdminModule\Components;

/**
 * Description of ICouponSettingsForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponSettingsForm
{

	/**
	 * @return CouponSettingsForm
	 */
	function create();

}
