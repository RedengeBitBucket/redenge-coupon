<?php

namespace Redenge\Coupon\AdminModule\Components;

use Exception;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Utils\Random;
use Redenge\Coupon\AdminModule\Helper\GridDictionary;
use Redenge\Engine\Components\BaseControl;
use ShopModel;
use Tracy\Debugger;
use Tracy\ILogger;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\DataSource\NetteDatabaseTableDataSource;
use Ublaboo\DataGrid\Localization\SimpleTranslator;


/**
 * Description of CouponItemControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponItemControl extends BaseControl
{

	/**
	 * @var null|int
	 */
	private $couponId;

	/**
	 * @var ShopModel
	 */
	private $model;

	/**
	 * @var Context
	 */
	private $db;


	/**
	 * @param ShopModel $model
	 */
	public function __construct($couponId, ShopModel $model, Context $db)
	{
		$this->couponId = $couponId;
		$this->model = $model;
		$this->db = $db;
	}


	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentGenerateForm()
	{
		$form = new Form();

		$form->addHidden('couponId', $this->couponId);
		$form->addText('count', 'Počet kupónů')
			->setAttribute('class', 'form-control');
		$form->addText('max_use', 'Počet použití')
			->setAttribute('class', 'form-control');
		$form->addSubmit('save', 'Uložit')->controlPrototype->class[] = 'btn btn-success ajax';
		$form->onSuccess[] = [$this, 'generateFormSuccess'];

		return $form;
	}


	/**
	 * @param \Nette\Application\UI\Form $form
	 *
	 * @throws \Nette\Application\AbortException
	 */
	public function generateFormSuccess(Form $form)
	{
		try {
			$count = $form->values->count;
			while ($count > 0) {
				$code = Random::generate(8, '0-9A-Z');
				$this->model->coupon->item->load([
					'code' => $code
				]);
				if ($this->model->coupon->item->id > 0) {
					continue;
				}
				$this->model->coupon->item->coupon_id = $this->couponId;
				$this->model->coupon->item->code = $code;
				$this->model->coupon->item->used = 0;
				$this->model->coupon->item->max_use = $form->values->max_use;
				$this->model->coupon->item->save();
				$count--;
			}

			$this->flashMessage('Kupóny uloženy', 'success');
			$this->redrawControl('formFlashes');
			$this['grid']->reload();
		}
		catch (AbortException $e) {
			throw $e;
		}
		catch (Exception $e) {
			$this->flashMessage('Kupóny se nepovedlo uložit', 'danger');
			$this->redrawControl('formFlashes');
		}
	}


	public function createComponentGrid()
	{
		$grid = new DataGrid();

		$grid->setTranslator(new SimpleTranslator(GridDictionary::cs()));
		$grid->setDataSource(new NetteDatabaseTableDataSource($this->db->table('coupon_item')->where('coupon_id = ?', $this->couponId), $grid->getPrimaryKey()));
		$grid->setItemsPerPageList([20, 50, 100, 200, 500]);
		$grid->setColumnsHideable();

		$grid->addColumnText('code', 'Kód')
			->setSortable();
		$grid->addColumnText('max_use', 'Počet použití')
			->setSortable();
		$grid->addColumnText('used', 'Použito')
			->setSortable();

		$grid->addFilterText('code', 'Kód');
		$grid->addFilterText('max_use', 'Počet použití');
		$grid->addFilterText('used', 'Použití');

		# Add new item
		$grid->addInlineAdd()
			->onControlAdd[] = function($container) {
			/** @var Container $container */
			$container->addText('code');
			$container->addText('max_use');
			$container->addText('used');
		};
		$grid->getInlineAdd()->onSetDefaults = function($container) {
			/** @var Container $container */
			$container->setDefaults([
				'used' => 0,
				'max_use' => 1
			]);
		};
		$grid->getInlineAdd()->onSubmit[] = [$this, 'addCouponItem'];
		$grid->getInlineAdd()->setPositionTop();

		# Edit product
		$grid->addInlineEdit()
			->onControlAdd[] = function($container) {
			/** @var Container $container */
			$container->addText('code');
			$container->addText('max_use');
			$container->addText('used');
		};
		$grid->getInlineEdit()->onSetDefaults[] = function($container, $item) {
			/** @var Container $container */
			$container->setDefaults([
				'code' => $item->code,
				'max_use' => $item->max_use,
				'used' => $item->used,
			]);
		};
		$grid->getInlineEdit()->onSubmit[] = [$this, 'editCouponItem'];

		$grid->addAction('deleteCouponItem!', '', null, ['couponItemId' => $grid->getPrimaryKey()])
			->setIcon('times')
			->setTitle('odstranit')
			->setClass('btn btn-xs btn-danger ajax');

		$grid->addGroupAction('Odstranit')
			->onSelect[] = [$this, 'deleteCouponItems'];

		$grid->addExportCsv('Export CSV', 'Kupony-' . date('H_i-d_m_Y') . '.csv')
			->setTitle('Export CSV')
			->setIcon('floppy-o');

		return $grid;
	}


	/**
	 * @param ArrayHash     $values
	 *
	 * @return void
	 */
	public function addCouponItem($values)
	{
		if ($this->db->table('coupon_item')->where('coupon_id', $this->couponId)->where('code', $values->code)->fetch()) {
			$this->flashMessage("Položka s kódem '{$values->code}' již existuje.", 'info');
		} else {
			$this->db->query('INSERT INTO coupon_item', [
				'coupon_id' => (int) $this->couponId,
				'code' => $values->code,
				'max_use' => $values->max_use,
				'used' => $values->used,
			]);

			$this->flashMessage("Položka s kódem '{$values->code}' byla přidána.", 'success');
			$this['grid']->reload();
		}

		$this->redrawControl('flashes');
	}


	/**
	 * @param int           $id
	 * @param ArrayHash     $values
	 *
	 * @return void
	 */
	public function editCouponItem($id, $values)
	{
		$this->db->table('coupon_item')->wherePrimary($id)->update([
			'code' => $values->code,
			'max_use' => $values->max_use,
			'used' => $values->used,
		]);
	}


	/**
	 * @param array $ids
	 *
	 * @return void
	 */
	public function deleteCouponItems(array $ids)
	{
		$this->db->table('coupon_item')->wherePrimary($ids)->delete();
		$this->flashMessage('Kupóny byly odstraněny', 'success');
		$this['grid']->reload();
	}


	/**
	 * @param int $couponItemId
	 */
	public function handleDeleteCouponItem($couponItemId)
	{
		if (!$this->db->table('coupon_item')->get($couponItemId)) {
			$this->flashMessage('Položka neexistuje.', 'danger');
		} else {
			$this->db->table('coupon_item')->wherePrimary($couponItemId)->delete();
			$this->flashMessage('Kupón byl odstraněn', 'success');
			$this['grid']->reload();
		}
		$this->redrawControl('flashes');
	}

}
