<?php

namespace Redenge\Coupon\AdminModule\Components;

/**
 * Description of ICouponBaseForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICouponBaseForm
{

	/**
	 * @return CouponBaseForm
	 */
	function create();

}
