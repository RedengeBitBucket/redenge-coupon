<?php

namespace Redenge\Coupon\AdminModule\Components;

use Exception;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Database\Context;
use Nette\Forms\Container;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextBase;
use Nette\Utils\DateTime;
use Kdyby\Events\EventArgsList;
use Kdyby\Events\EventManager;
use Redenge\Engine\Components\BaseControl;
use ShopModel;
use Tracy\Debugger;
use Tracy\ILogger;


/**
 * Description of CouponBaseForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponBaseForm extends BaseControl
{

	/**
	 * @var null|int
	 */
	private $id;

	/**
	 * @var EventManager
	 */
	private $evm;

	/**
	 * @var ShopModel
	 */
	private $model;

	/**
	 * @var Context
	 */
	private $db;


	/**
	 * @var null|callable
	 */
	public $onSuccess;

	/**
	 * @var null|callable
	 */
	public $onError;


	/**
	 * @param ShopModel $model
	 */
	public function __construct(EventManager $evm, ShopModel $model, Context $db)
	{
		$this->model = $model;
		$this->evm = $evm;
		$this->db = $db;
	}


	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		$form = new Form();

		$form->addText('code', 'Kód')
			->setRequired('Povinné pole');

		$form->addText('name', 'Název')
			->setRequired('Povinné pole');

		$form->addSelect('type', 'Typ', $this->getTypes());

		$form->addSelect('validtype', 'Aktivní pro', $this->getValidTypes());

		$form->addSelect('multishop_id', 'Multishop', $this->getMultishops());

		$form->addCheckbox('active', 'Aktivní');

		$form->addText('number_of_uses', 'Počet použití (0 = neomezeno)')
			->setValue(0);

		$form->addDatePicker('valid_from', 'Platnost od')
			->setValue(new DateTime('now'));

		$form->addDatePicker('valid_to', 'Platnost do')
			->setValue(new DateTime('now'));

		$form->addSubmit('save', 'Uložit')->controlPrototype->class[] = 'btn btn-sm btn-success';
		$form->addButton('back', 'Zpět na výpis')
			->setAttribute('onclick', sprintf('window.location.href="%s"', $this->presenter->link('default')))
			->setAttribute('class', 'btn btn-sm btn-primary');

		$customParamsContainer = $form->addContainer('custom_params');
		$this->evm->dispatchEvent('Redenge\Coupon\AdminModule\Components\CouponBaseForm::onCreateCustomParams', new EventArgsList([$customParamsContainer]));

		foreach ($customParamsContainer->getControls() as $control) {
			switch (true) {
				case $control instanceof \Nette\Forms\Controls\TextInput:
					$this->model->coupon->createString($control->getName());
					break;
				case $control instanceof \Nette\Forms\Controls\Checkbox:
					$this->model->coupon->createBool($control->getName());
					break;
			}
		}

		$form->onSuccess[] = [$this, 'save'];

		foreach ($form->getControls() as $control)
			if ($control instanceof TextBase || $control instanceof SelectBox)
				$control->controlPrototype->class[] = 'form-control';
				
		$form->setDefaults($this->getDefaults($this->id, $customParamsContainer));

		return $form;
	}


	/**
	 * @param null|int $id
	 *
	 * @return array
	 */
	private function getDefaults($id, Container $customParamsContainer)
	{
		$coupon = $this->model->coupon;
		$defaults = [];

		if (!$id || !$coupon->load(['id' => $id])->id) {
			return [];
		}

		$defaults['code'] = $coupon->code;
		$defaults['name'] = $coupon->name;
		$defaults['type'] = $coupon->type;
		$defaults['validtype'] = $coupon->validtype;
		$defaults['multishop_id'] = $coupon->multishop_id;
		$defaults['active'] = $coupon->active;
		$defaults['number_of_uses'] = $coupon->number_of_uses;
		$defaults['valid_from'] = ((int) $coupon->valid_from) ? new DateTime($coupon->valid_from) : null;
		$defaults['valid_to'] = ((int) $coupon->valid_to) ? new DateTime($coupon->valid_to) : null;

		foreach ($customParamsContainer->getControls() as $control) {
			if (isset($coupon->{$control->getName()})) {
				$defaults[$customParamsContainer->getName()][$control->getName()] = $coupon->{$control->getName()};
			}
		}

		return $defaults;
	}


	/**
	 * @return array
	 */
	private function getTypes()
	{
		return [
			1 => 'Sleva na nákup v % nebo hodnotová při nákupu nad hodnotu v košíku'
		];
	}


	/**
	 * @return array
	 */
	private function getValidTypes()
	{
		return [
			1 => 'Pouze produkty',
			2 => 'Pouze kategorie',
			3 => 'Pouze atributy',
			4 => 'Pouze vlastnosti',
			5 => 'Kombinace kategorie a atributu',
			6 => 'Kombinace kategorie a vlastnosti',
			7 => 'Celý sortiment',
		];
	}


	/**
	 * @return array
	 */
	private function getMultishops()
	{
		return $this->db->table('multishop')->fetchPairs('id', 'code');
	}


	public function save(Form $form)
	{
		$coupon = $this->model->coupon;
		$edit = (boolean) $coupon->id;

		try {
			$values = $form->getValues();

			$coupon->code = $values->code;
			$coupon->name = $values->name;
			$coupon->type = $values->type;
			$coupon->validtype = $values->validtype;
			$coupon->multishop_id = $values->multishop_id;
			$coupon->active = $values->active;
			$coupon->number_of_uses = $values->number_of_uses;
			$coupon->valid_from = ($values->valid_from instanceof DateTime) ? $values->valid_from->format('Y-m-d H:i:s') : '0000-00-00';
			$coupon->valid_to = ($values->valid_to instanceof DateTime) ? $values->valid_to->format('Y-m-d H:i:s') : '0000-00-00';

			if (!empty($values['custom_params'])) {
				foreach ($values['custom_params'] as $columnName => $columnValue) {
					if (!isset($coupon->{$columnName})) {
						continue;
					}
					$coupon->{$columnName} = $columnValue;
				}
			}
			$coupon->save();

			if (is_callable($this->onSuccess)) {
				$this->onSuccess($coupon->id, $edit);
			}
		} catch (AbortException $e) {
			throw $e;
		} catch (Exception $e) {
			Debugger::log($e->getMessage(), ILogger::EXCEPTION);
			if (is_callable($this->onError)) {
				$this->onError($edit);
			}
		}
	}

}
