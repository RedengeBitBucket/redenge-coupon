<?php

namespace Redenge\Coupon\AdminModule\Components;

use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Redenge\Engine\Components\BaseControl;
use Redenge\Engine\Tools\CategoryGroupHelper;
use ShopModel;
use Tracy\Debugger;
use Tracy\ILogger;


class CategoryTreeForm extends BaseControl
{
	/**
	 * @var int
	 */
	private $couponId;

	/**
	 * @var \ShopModel
	 */
	private $model;

	/**
	 * @var callable
	 */
	public $onSuccess;

	/**
	 * @var callable
	 */
	public $onError;


	/**
	 * @param int                                  $couponId
	 * @param ShopModel                            $model
	 */
	public function __construct($couponId, ShopModel $model)
	{
		$this->couponId = $couponId;
		$this->model = $model;
	}

	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		$form = new Form();

		$form->addHidden('couponId', $this->couponId);
		$form->addSubmit('save', 'Uložit')->controlPrototype->class[] = 'btn btn-success';
		$form->onSuccess[] = [$this, 'save'];

		return $form;
	}

	/**
	 * @param \Nette\Application\UI\Form $form
	 *
	 * @throws \Nette\Application\AbortException
	 */
	public function save(Form $form)
	{
		try {
			$this->model->getDb()->beginTransaction();
			$values = $form->values;
			$categories = $form->getHttpData($form::DATA_TEXT | $form::DATA_KEYS, 'categories[]');

			$this->model->coupon->category->insert($values->couponId, $categories);
			$this->model->coupon->category->deleteNotIn($values->couponId, $categories);

			$this->model->getDb()->commit();
			if (is_callable($this->onSuccess))
				$this->onSuccess();
		}
		catch (AbortException $e) {
			throw $e;
		}
		catch (\Exception $e) {
			Debugger::log($e->getMessage(), ILogger::EXCEPTION);
			$this->model->getDb()->rollback();
			if (is_callable($this->onError))
				$this->onError();
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function render()
	{
		$groupRecords = $this->model->category_group->getRecords('id', null, 'id');

		$groups = [];
		foreach ($groupRecords->fetch_all(MYSQLI_ASSOC) as $group) {
			$groups[$group['id']] = [];
			$select = 'IF(cc.id IS NULL, 0, 1) AS active, category_attribute.name, category.id AS current, category.id_category as parent';
			$join = 'JOIN category_attribute ON category_attribute.id_category = category.id AND category_attribute.id_language = 1';
			$join .= ' JOIN category_group ON category_group.id = category.id_category_group AND category_group.id = ' . $group['id'];
			$join .= ' LEFT JOIN coupon_category cc ON cc.category_id = category.id AND cc.coupon_id = ' . $this->couponId;
			$order = 'category.id';
			$groupBy = 'category.id ASC';
			$tree = $this->model->category->getRecords($select, $join, $order, null, $groupBy);
			$tmp = [];
			$categories = [];

			while ($r = $tree->fetch_assoc()) {
				$currid = $r["parent"];

				$category = new CategoryGroupHelper($r["current"], $currid, '', $r["name"], 0, 0, 0);
				$category->setActive((boolean) $r['active']);

				if (!array_key_exists($r["current"], $tmp))
					$tmp[$r["current"]] = [];

				$category->children = &$tmp[$r["current"]];

				if (!array_key_exists($currid, $tmp))
					$tmp[$currid] = [];

				if ($currid == 0)
					array_push($categories, $category);
				else
					array_push($tmp[$currid], $category);
			}
			$groups[$group['id']] = $categories;

			$tree->free_result();
		}

		$this->template->groups = ArrayHash::from($groups);
		$groupRecords->free_result();

		parent::render();
	}

}
