<?php

namespace Redenge\Coupon\AdminModule\Components;

/**
 * Description of ICategoryTreeForm
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface ICategoryTreeForm
{

	/**
	 * @return CategoryTreeForm
	 */
	function create($couponId);

}
