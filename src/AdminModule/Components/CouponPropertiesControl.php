<?php

namespace Redenge\Coupon\AdminModule\Components;

use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Database\Context;
use Nette\Utils\Html;
use Redenge\Coupon\AdminModule\Components\ICategoryTreeForm;
use Redenge\Coupon\AdminModule\Helper\GridDictionary;
use Redenge\Engine\Components\BaseControl;
use ShopModel;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\DataSource\NetteDatabaseTableDataSource;
use Ublaboo\DataGrid\Localization\SimpleTranslator;


/**
 * Description of CouponPropertiesControl
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class CouponPropertiesControl extends BaseControl
{

	/**
	 * @var ShopModel
	 */
	private $model;

	/**
	 * @var Context
	 */
	private $db;


	/**
	 * @param ShopModel $model
	 */
	public function __construct(ShopModel $model, Context $db)
	{
		$this->model = $model;
		$this->db = $db;
	}


	public function handleDeleteProperty($propertyId)
	{
		if (!$this->db->table('coupon_property')->where('coupon_id = ? AND property_id = ?', $this->presenter->getParameter('id'), $propertyId)->count()) {
			$this->flashMessage('Položka neexistuje.', 'danger');
		} else {
			$this->db->table('coupon_property')->where('coupon_id = ? AND property_id = ?', $this->presenter->getParameter('id'), $propertyId)->delete();
			$this->flashMessage('Položka byla odstraněna', 'success');
			$this['propertiesGrid']->reload();
		}
		$this->redrawControl('flashes');
	}


	public function changeActive($propertyId, $newStatus)
	{
		$conditions = ['property_id' => $propertyId, 'coupon_id' => $this->presenter->getParameter('id')];
		if ((int)$newStatus === 1) {
			if ($this->db->table('coupon_property')->where($conditions)->count() === 0) {
				$this->db->query('INSERT INTO coupon_property', [
					'property_id' => $propertyId,
					'coupon_id' => $this->presenter->getParameter('id'),
				]);
			}
		} else {
			$this->db->table('coupon_property')->where($conditions)->delete();
		}
		if ($this->presenter->isAjax()) {
			$this['propertiesGrid']->redrawItem($propertyId, 'id');
		}
	}


	public function editProperty($id, $values)
	{
		$postParams = $this->presenter->getRequest()->getPost('inline_edit');
		if (!isset($postParams['value'])) {
			$this->changeActive($id, 0);
			return;
		}
		$type = $this->getPropertyType($id);
		$couponId = $this->presenter->getParameter('id');
		$archetypeId = $this->getArchetypeId($values->archetype);
		switch ($type) {
			case 'COMBO':
			case 'SELECT':
				$propertyValueIds = [];
				if ($type === 'COMBO') {
					$propertyValueIds[] = (int) $postParams['value'];
				} elseif ($type === 'SELECT') {
					$propertyValueIds = $postParams['value'];
				}

				foreach ($propertyValueIds as $propertyValueId) {
					$this->model->coupon->property->load([
						'coupon_id' => $couponId,
						'archetype_id' => $archetypeId,
						'property_id' => $id,
						'property_value_id' => (int) $propertyValueId
					]);
					$this->model->coupon->property->coupon_id = $couponId;
					$this->model->coupon->property->archetype_id = $archetypeId;
					$this->model->coupon->property->property_id = $id;
					$this->model->coupon->property->property_value_id = (int) $propertyValueId;
					$this->model->coupon->property->save();
				}

				$this->model->coupon->property->deleteValuesNotIn($couponId, $archetypeId, $id, $propertyValueIds);
				break;
			case 'BIT':
				$postParams['value'] = $postParams['value'] == 'on' ? 1 : 0;
			default:
				$this->model->coupon->property->load(['coupon_id' => $this->presenter->getParameter('id'), 'property_id' => $id]);
				$this->model->coupon->property->coupon_id = $this->presenter->getParameter('id');
				$this->model->coupon->property->archetype_id = $archetypeId;
				$this->model->coupon->property->property_id = $id;
				$this->model->coupon->property->value = $postParams['value'];
				$this->model->coupon->property->save();
		}

		if ($this->presenter->isAjax()) {
			$this['propertiesGrid']->redrawItem($id, 'id');
		}
	}


	public function createComponentPropertiesGrid()
	{
		$grid = new DataGrid();
		$grid->setTranslator(new SimpleTranslator(GridDictionary::cs()));
		$grid->setDataSource(new NetteDatabaseTableDataSource(
			$this->db->table('property')
				->select('property.id, :archetype_property.archetype.name archetype, :archetype_property.id_archetype
					, :property_attribute.name, property_type.code AS type, IF(:coupon_property.property_id IS NULL, 0, 1) active
					, IFNULL(:coupon_property.value, \'\') value')
				->where(':coupon_property.coupon_id ? OR :coupon_property.coupon_id IS NULL', $this->presenter->getParameter('id'))
				->group(':archetype_property.id')
				->order(':property_attribute.name')
			, $grid->getPrimaryKey()
		));

		$grid->setItemsPerPageList([20, 50, 100, 200, 500]);
		$grid->setColumnsHideable();

		$grid->addColumnText('archetype', 'Archetyp')
			->setSortable();
		$grid->addFilterText('archetype', 'Archetyp', ':archetype_property.archetype.name');

		$grid->addColumnText('name', 'Vlastnost')
			->setSortable();
		$grid->addFilterText('name', 'Vlastnost', ':property_attribute.name');

		$grid->addColumnText('value', 'Hodnota');
		$grid->addColumnCallback('value', function($column, $item) {
			switch ($item->type) {
				case 'INT':
				case 'REAL':
					$column->setTemplate(__DIR__ . '/templates/grid_column_text.latte', [
						'column' => $column,
						'value' => $item->value
					]);
					break;
				case 'BIT':
					$attrs = [];
					if ($item->value == 1) {
						$attrs['checked'] = 'checked';
					}
					$column->setTemplate(__DIR__ . '/templates/grid_column_checkbox.latte', [
						'column' => $column,
						'attrs' => $attrs,
						'value' => $item->value
					]);
					break;
				case 'COMBO':
				case 'SELECT':
					$attrs = [];
					if ($item->type === 'SELECT') {
						$attrs['multiple'] = 'multiple';
					}
					$values = $this->model->property_value->getPropertyValues($item->id, 1);
					$activeValues = $this->model->coupon->property->getPropertyValues($this->presenter->getParameter('id'), $item->id, $item->id_archetype);

					$column->setTemplate(__DIR__ . '/templates/grid_column_multiselect.latte', [
						'column' => $column,
						'values' => $values,
						'activeValues' => $activeValues,
						'attrs' => $attrs
					]);
					break;
				default:
					$column->setTemplate(__DIR__ . '/templates/grid_column_text.latte', [
						'column' => $column,
						'value' => $item->value
					]);
			}
		});

		$grid->addColumnText('active', 'Přiřazeno')
			->setSortable()
			->setRenderer(function($item) {
				$el = Html::el('span', [
					'class' => $item->active == 1 ? 'btn btn-xs btn-success' : 'btn btn-xs btn-danger',
					'style' => 'cursor: default;'
				]);

				return $el->setText($item->active == 1 ? 'Ano' : 'Ne');
			});

		$grid->addFilterSelect('active', null, ['' => 'Vše', 1 => 'Ano', 0 => 'Ne'], 'IF(:coupon_property.property_id IS NULL, 0, 1)');

		$grid->addInlineEdit('property.id')
			->onControlAdd[] = function($container) {
			$container->addText('archetype')->setAttribute('readonly');
			$container->addText('name')->setAttribute('readonly');
		};
		$grid->getInlineEdit()->onSetDefaults[] = function($container, $item) {
			switch ($item->type) {
				case 'INT':
				case 'REAL':
					$container->addText('value')
						->setAttribute('class', 'form-control')
						->setValue($item->value);
					break;
				case 'BIT':
					$container->addCheckbox('value')
						->setAttribute('class', 'form-check-input')
						->setDefaultValue(1);
					if ($item->value == 1) {
						$container['value']->setAttribute('checked', 'checked');
					}
					break;
				case 'COMBO':
				case 'SELECT':
					$items = $this->model->property_value->getPropertyValues($item->id, 1);
					$activeItems = $this->model->coupon->property->getPropertyValues($this->presenter->getParameter('id'), $item->id, $item->id_archetype);
					if ($item->type === 'SELECT') {
						$container->addMultiSelect('value', null, $items)
							->setAttribute('class', 'form-control')
							->setAttribute('style', 'height: auto;')
							->setValue($activeItems);
					} else {
						$container->addSelect('value', null, $items)
							->setAttribute('class', 'form-control');

						$value = array_pop($activeItems);
						if ($value) {
							$container['value']->setValue($value);
						}
						
					}
					break;
				default:
					$container->addText('value')
						->setAttribute('class', 'form-control')
						->setValue($item->value);
						
			}

			$container->setDefaults([
				'archetype' => $item->archetype,
				'name' => $item->name,
			]);
		};
		$grid->getInlineEdit()->onSubmit[] = [$this, 'editProperty'];

		# Delete product
		$grid->addAction('deleteProperty!', '', null, ['propertyId' => $grid->getPrimaryKey()])
			->setIcon('times')
			->setTitle('odstranit')
			->setClass('btn btn-xs btn-danger ajax');

		return $grid;
	}


	private function getPropertyType($propertyId)
	{
		return $this->db->table('property')->select('property_type.code')->where("property.id = ?", $propertyId)->fetchField() ?: '';
	}


	private function getArchetypeId($archetypeName)
	{
		return $this->db->table('archetype')->select('id')->where("archetype.name = ?", $archetypeName)->fetchField() ?: 0;
	}

}
