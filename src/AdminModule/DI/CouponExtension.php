<?php

namespace Redenge\Coupon\AdminModule\DI;

use Nette\Application\IPresenterFactory;
use Nette\DI\CompilerExtension;


class CouponExtension extends CompilerExtension
{

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$builder->getDefinition($builder->getByType(IPresenterFactory::class))->addSetup(
			'setMapping',
			[['Coupon' => 'Redenge\Coupon\AdminModule\Presenters\*Presenter']]
		);

		$this->compiler->parseServices($builder, $this->loadFromFile(__DIR__ . '/../config/services.neon'), $this->name);
	}

}
